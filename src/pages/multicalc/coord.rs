use std::{collections::VecDeque, fmt, num::ParseIntError};

use crate::{
    sanitize::{Sanitize, FormatError},
    pages::multicalc::{
        calcwrapper,
        variable::VariableKind,
        value::Value,
    }
};

use lazy_static::lazy_static;
use regex::Regex;
use serde::{Serialize, Deserialize};

pub trait CoordinatePart: CoordinatePartInner {
    fn valid_directions() -> [Direction; 2];

    fn max_degrees() -> i32;
}

/// Used to describe a direction on earth
#[derive(Clone, Copy, Debug, Deserialize, PartialEq, Serialize)]
pub enum Direction {
    /// North
    North,
    /// East
    East,
    /// South
    South,
    /// West
    West,
}

/// Representation for the minutes part of the coordinates
#[derive(Clone, Debug, PartialEq)]
pub struct CoordinateMinutes {
    /// whole minutes (**xx**.yyy)
    minutes: i32,
    /// sub-minutes (xx.**yyy**)
    subminutes: i32,
    /// additional formula
    formula: String,
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct Latitude {
    direction: Direction,
    degrees: i32,
    minutes_input: String,
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct Longitude {
    direction: Direction,
    degrees: i32,
    minutes_input: String,
}

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct CalculatedCoordinate {
    lat: CalculatedCoordinatePart,
    lon: CalculatedCoordinatePart,
}

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct CalculatedCoordinatePart {
    direction: Direction,
    degrees: i32,
    minutes: i32,
    subminutes: i32,
}


#[derive(Clone, Debug, PartialEq)]
pub enum CoordinateError {
    Calculation(String),
    Input,
    ParseInt(ParseIntError),
}

impl From<ParseIntError> for CoordinateError {
    fn from(error: ParseIntError) -> Self {
        Self::ParseInt(error)
    }
}

pub trait CoordinatePartInner: private::Sealed {
    fn direction(&self) -> Direction;

    fn degrees(&self) -> i32;

    fn minutes_input(&self) -> &str;

    fn minutes_input_owned(&self) -> String {
        self.minutes_input().to_string()
    }

    fn set_direction(&mut self, direction: Direction);

    fn set_degrees(&mut self, degrees: i32);

    fn set_minutes_input(&mut self, minutes_input: String);

    fn minutes(&self) -> Result<CoordinateMinutes, CoordinateError> {
        let input = self.minutes_input();

        lazy_static! {
            static ref RE: Regex = Regex::new(
                r"(?x)
                \s*(?P<minutes>\d{0,2})         # minutes
                \s*(?P<separator>[\.,]{0,1})    # optional separator
                \s*(?P<subminutes>\d{0,3})      # sub-minutes (allow less than 3 characters so that we dont fail to
                                                # parse it while the user is still entering)
                (?P<formula>.*)                 # remainder is the formula
                "
            )
            .unwrap();
        }

        if input.trim().is_empty() {
            return Ok(CoordinateMinutes {
                minutes: 0,
                subminutes: 0,
                formula: String::new(),
            })
        }

        match RE.captures(input) {
            Some(caps) => {
                let mut minutes = match caps.name("minutes") {
                    Some(minutes) => {
                        if minutes.as_str().trim().is_empty() {
                            "0"
                        } else {
                            minutes.as_str()
                        }.parse::<i32>()?
                    },
                    None => 0,
                };

                let (mut subminutes, concat) = match caps.name("subminutes") {
                    Some(subminutes) => {
                        if subminutes.as_str().trim().is_empty() {
                            (0, false)
                        } else {
                            let string =subminutes.as_str();
                            (string.parse::<i32>()?, string.len() < 3)
                        }
                    },
                    None => (0, false),
                };

                let mut formula = caps.name("formula")
                    .map(|formula| formula.as_str())
                    .unwrap_or("")
                    .to_string();

                match formula.chars().position(|cc| cc == '.') {
                    // ugly hack. if there's a . at the beginning of the formula, assume that
                    // everything before that belongs to the minute part. This will fix problems
                    // with things like "1A.BCD" which would otherwise incorrectly handle the A
                    Some(idx) if idx <= 5 => {
                        let mut segments = formula.split('.');
                        formula = format!(
                            "({}{})*1000 + {}",
                            minutes,
                            segments.next().unwrap(),
                            segments.next().unwrap_or("")
                        );
                        minutes = 0;
                    }
                    _ => {
                        formula = formula.to_string().replace('.', "");
                    }
                }

                // for things like 12.3AB
                if concat {
                    formula = format!("{}{}", subminutes, formula);
                    subminutes = 0;
                }

                Ok(CoordinateMinutes {
                    minutes,
                    subminutes,
                    formula,
                })
            }
            None => Err(CoordinateError::Input),
        }
    }

    fn value<S: AsRef<str>>(&self, variables: &VecDeque<(S, Value<i32>, VariableKind)>)
    -> Result<Value<CalculatedCoordinatePart>, CoordinateError> {
        let minutes = self.minutes()?;

        if minutes.formula.trim().is_empty() {
            Ok(Value::new(CalculatedCoordinatePart {
                direction: self.direction(),
                degrees: self.degrees(),
                minutes: minutes.minutes,
                subminutes: minutes.subminutes,
            }))
        } else {
            let mut formula = match minutes.formula.trim().chars().next() {
                Some('+' | '-' | '*' | '/') => {
                    format!(
                        "{}{:03}{}",
                        minutes.minutes,
                        minutes.subminutes,
                        minutes.formula,
                    )
                },
                Some('(') => {
                    if minutes.minutes == 0 && minutes.subminutes == 0 {
                        minutes.formula.clone()
                    } else {
                        format!(
                            "{}{:03}+{}",
                            minutes.minutes,
                            minutes.subminutes,
                            minutes.formula,
                        )
                    }
                }
                _ if minutes.subminutes == 0 => format!(
                    "({} * 1000) + {}",
                    minutes.minutes,
                    minutes.formula,
                ),
                _ => format!(
                    "{}{}{}",
                    minutes.minutes,
                    minutes.subminutes,
                    minutes.formula,
                ),
            };
            let mut info = None;

            for (varname, value, _) in variables {
                if formula.contains(varname.as_ref()) && !value.is_value_set() {
                    info = Some("Uses variable which doesn't exist or cannot be calculated".to_string());
                }
                formula = formula.replace(varname.as_ref(), &value.value().to_string());
            }
            match calcwrapper::evaluate(&formula) {
                Ok(result) => {
                    Ok(Value::new(CalculatedCoordinatePart {
                        direction: self.direction(),
                        degrees: self.degrees(),
                        minutes: (result / 1000) as i32,
                        subminutes: (result % 1000) as i32,
                    }).with_info(info))
                }
                Err(err) => Err(CoordinateError::Calculation(format!("{}", err))),
            }
        }
    }
}

impl Default for Direction {
    fn default() -> Self {
        Self::North
    }
}

mod private {
    pub trait Sealed {}

    impl Sealed for super::Latitude {}
    impl Sealed for super::Longitude {}
}

impl CoordinatePartInner for Latitude {
    fn direction(&self) -> Direction {
        self.direction
    }

    fn degrees(&self) -> i32 {
        self.degrees
    }

    fn minutes_input(&self) -> &str {
        &self.minutes_input
    }

    fn set_direction(&mut self, direction: Direction) {
        if Self::valid_directions().contains(&direction) {
            self.direction = direction;
        }
    }

    fn set_degrees(&mut self, degrees: i32) {
        self.degrees = if degrees > 0 {
            i32::min(degrees, Self::max_degrees())
        } else {
            0
        }
    }

    fn set_minutes_input(&mut self, minutes_input: String) {
        self.minutes_input = minutes_input.to_ascii_uppercase();
    }
}

impl CoordinatePartInner for Longitude {
    fn direction(&self) -> Direction {
        self.direction
    }

    fn degrees(&self) -> i32 {
        self.degrees
    }

    fn minutes_input(&self) -> &str {
        &self.minutes_input
    }

    fn set_direction(&mut self, direction: Direction) {
        if Self::valid_directions().contains(&direction) {
            self.direction = direction;
        }
    }

    fn set_degrees(&mut self, degrees: i32) {
        self.degrees = if degrees > 0 {
            i32::min(degrees, Self::max_degrees())
        } else {
            0
        }
    }

    fn set_minutes_input(&mut self, minutes_input: String) {
        self.minutes_input = minutes_input.to_ascii_uppercase();
    }
}

impl CoordinatePart for Latitude {
    fn valid_directions() -> [Direction; 2] {
        [Direction::North, Direction::South]
    }

    fn max_degrees() -> i32 {
        90
    }
}

impl CoordinatePart for Longitude {
    fn valid_directions() -> [Direction; 2] {
        [Direction::East, Direction::West]
    }

    fn max_degrees() -> i32 {
        180
    }
}

impl Default for Latitude {
    fn default() -> Self {
        Self {
            direction: Direction::North,
            degrees: 0,
            minutes_input: String::new(),
        }
    }
}

impl Default for Longitude {
    fn default() -> Self {
        Self {
            direction: Direction::East,
            degrees: 0,
            minutes_input: String::new(),
        }
    }
}

impl Sanitize for Latitude {
    fn sanitized(mut self) -> (Self, Vec<(&'static str, FormatError)>) {
        let mut errors = Vec::new();

        if !Self::valid_directions().contains(&self.direction) {
            errors.push(("direction", FormatError::InvalidValue));
            self.direction = Self::valid_directions()[0];
        }

        self.degrees = if self.degrees < 0 {
            errors.push(("degrees", FormatError::InvalidValue));
            0
        } else if self.degrees > Self::max_degrees() {
            errors.push(("degrees", FormatError::InvalidValue));
            Self::max_degrees()
        } else {
            self.degrees
        };

        self.minutes_input = self.minutes_input.to_ascii_uppercase();

        (self, errors)
    }
}

impl Sanitize for Longitude {
    fn sanitized(mut self) -> (Self, Vec<(&'static str, FormatError)>) {
        let mut errors = Vec::new();

        if !Self::valid_directions().contains(&self.direction) {
            errors.push(("direction", FormatError::InvalidValue));
            self.direction = Self::valid_directions()[0];
        }

        self.degrees = if self.degrees < 0 {
            errors.push(("degrees", FormatError::InvalidValue));
            0
        } else if self.degrees > Self::max_degrees() {
            errors.push(("degrees", FormatError::InvalidValue));
            Self::max_degrees()
        } else {
            self.degrees
        };

        self.minutes_input = self.minutes_input.to_ascii_uppercase();

        (self, errors)
    }
}

impl fmt::Display for CalculatedCoordinate {
    fn fmt(&self, ff: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(ff, "{} {}", self.lat, self.lon)
    }
}

impl fmt::Display for CalculatedCoordinatePart {
    fn fmt(&self, ff: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            ff,
            "{} {:0width_degrees$}° {:02}.{:03}",
            self.direction,
            self.degrees,
            self.minutes,
            self.subminutes,
            width_degrees = { if Latitude::valid_directions().contains(&self.direction) {
                2
            } else {
                3
            }},
        )
    }
}

impl fmt::Display for Direction {
    fn fmt(&self, ff: &mut fmt::Formatter<'_>) -> fmt::Result {
        let letter = match self {
            Self::North => 'N',
            Self::East => 'E',
            Self::South => 'S',
            Self::West => 'W',
        };
        write!(ff, "{}", letter)
    }
}

impl std::str::FromStr for Direction {
    type Err = String;

    fn from_str(source: &str) -> Result<Self, Self::Err> {
        let lower = source.to_lowercase();

        match lower.as_str() {
            "n" | "north" => Ok(Self::North),
            "s" | "south" => Ok(Self::South),
            "w" | "west" => Ok(Self::West),
            "e" | "east" => Ok(Self::East),
            other => Err(format!("{} is not a valid direction pattern", other)),
        }
    }
}

impl Latitude {
    #[cfg(test)]
    pub fn with(direction: Direction, degrees: i32, minutes_input: &str) -> Self {
        Self {
            direction,
            degrees,
            minutes_input: minutes_input.to_string(),
        }
    }
}

impl Longitude {
    #[cfg(test)]
    pub fn with(direction: Direction, degrees: i32, minutes_input: &str) -> Self {
        Self {
            direction,
            degrees,
            minutes_input: minutes_input.to_string(),
        }
    }
}

impl CalculatedCoordinate {
    pub fn new(lat: CalculatedCoordinatePart, lon: CalculatedCoordinatePart) -> Self {
        Self { lat, lon }
    }

    pub fn lat(&self) -> &CalculatedCoordinatePart {
        &self.lat
    }

    pub fn lon(&self) -> &CalculatedCoordinatePart {
        &self.lon
    }
}

impl CalculatedCoordinatePart {
    pub fn direction(&self) -> Direction {
        self.direction
    }

    #[allow(dead_code)]
    pub fn degrees(&self) -> i32 {
        self.degrees
    }

    pub fn minutes(&self) -> i32 {
        self.minutes
    }

    pub fn subminutes(&self) -> i32 {
        self.subminutes
    }

    #[cfg(test)]
    pub fn with(direction: Direction, degrees: i32, minutes: i32, subminutes: i32) -> Self {
        Self {
            direction,
            degrees,
            minutes,
            subminutes,
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::{assert_eq};

    #[test]
    fn minutes_extraction() {
        let data = [
            ("12.000", Ok(CoordinateMinutes { minutes: 12, subminutes: 0, formula: String::new() })),
            ("12,000", Ok(CoordinateMinutes { minutes: 12, subminutes: 0, formula: String::new() })),
            ("12000", Ok(CoordinateMinutes { minutes: 12, subminutes: 0, formula: String::new() })),
            (" 12 . 000", Ok(CoordinateMinutes { minutes: 12, subminutes: 0, formula: String::new() })),

            ("12.345", Ok(CoordinateMinutes { minutes: 12, subminutes: 345, formula: String::new() })),
            ("12,345", Ok(CoordinateMinutes { minutes: 12, subminutes: 345, formula: String::new() })),
            ("12345", Ok(CoordinateMinutes { minutes: 12, subminutes: 345, formula: String::new() })),
            (" 12 . 345 ", Ok(CoordinateMinutes { minutes: 12, subminutes: 345, formula: " ".to_string() })),
        ];

        for (input, expected) in data {
            let coord = Latitude {
                direction: Direction::North,
                degrees: 0,
                minutes_input: input.to_string(),
            };

            let result = coord.minutes();
            assert_eq!(expected, result, "failed for input: {}", input);
        }
    }
}
