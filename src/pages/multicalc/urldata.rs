use crate::{sanitize::Sanitize, util::GeoCode};
use super::{checksum::Checksum, MultiCache, Stage, Variable};

use serde::{Serialize, Deserialize};


#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct Urldata {
    #[serde(flatten)]
    id: GeoCode,
    name: String,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    variables: Vec<Variable>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    checksum: Option<Checksum>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    stages: Vec<Stage>,
    version: UrldataVersion,
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub enum UrldataVersion {
    V0_1,
}

impl Urldata {
    pub(super) fn multicache_from_json(from: &str) -> Result<MultiCache, serde_json::Error> {
        let urldata: Self = serde_json::from_str(from)?;
        Ok(urldata.into())
    }

    pub(super) fn multicache_from_urlencoded_json(from: &str) -> Option<MultiCache> {
        // also accept it if the hash is directly provided
        let hash = from.strip_prefix('#').unwrap_or(from);

        match urlencoding::decode(hash) {
            Ok(json) => Self::multicache_from_json(&json).ok(),
            Err(_) => None
        }
    }

    pub fn into_json(self) -> String {
        serde_json::to_string(&self).unwrap_or_else(|_| "Failed to serialize".to_string())
    }

    #[allow(dead_code)]
    pub fn into_json_pretty(self) -> String {
        serde_json::to_string_pretty(&self).unwrap_or_else(|_| "Failed to serialize".to_string())
    }
}

impl From<MultiCache> for Urldata {
    fn from(multi: MultiCache) -> Self {
        Self {
            id: multi.id,
            name: multi.name,
            variables: multi.variables,
            checksum: multi.checksum,
            stages: multi.stages,
            version: UrldataVersion::V0_1,
        }
    }
}

impl From<&MultiCache> for Urldata {
    fn from(multi: &MultiCache) -> Self {
        Self {
            id: multi.id.to_owned(),
            name: multi.name.to_owned(),
            variables: multi.variables.to_owned(),
            checksum: multi.checksum.clone(),
            stages: multi.stages.to_owned(),
            version: UrldataVersion::V0_1,
        }
    }
}

// Allowed because we need to implement it ourself to call the sanitized() function on members
#[allow(clippy::from_over_into)]
impl Into<MultiCache> for Urldata {
    fn into(self) -> MultiCache {
        MultiCache {
            id: self.id.sanitized().0,
            name: self.name,
            variables: self.variables.into_iter().map(|var| var.sanitized().0).collect(),
            stages: self.stages.into_iter().map(|stage| stage.sanitized().0).collect(),
            checksum: self.checksum,
        }
    }
}
