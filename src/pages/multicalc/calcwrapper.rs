use std::fmt;

use calc::{Context, Error as CalcError};

#[derive(Debug)]
pub enum EvaluationError {
    Calc(CalcError<i64>),
    Syntax(String),
}

impl From<CalcError<i64>> for EvaluationError {
    fn from(calc_error: CalcError<i64>) -> Self {
        Self::Calc(calc_error)
    }
}

impl fmt::Display for EvaluationError {
    fn fmt(&self, ff: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Calc(calc_error) => write!(ff, "Calculation Error: {}", calc_error),
            Self::Syntax(reason) => write!(ff, "Syntax Error: {}", reason),
        }
    }
}

/// A wrapper function aroung `calc::Context::evaluate` which handles the case of a calculation returning decimal
/// numbers
pub fn evaluate(formula: &str) -> Result<i64, EvaluationError> {
    // Even though we are interested in i64 numbers, we need to calculate f64 first. This is necessary because some
    // divisions might just lead to a 0 value.
    match Context::<f64>::default().evaluate(formula) {
        Ok(f64_result) => {
            // if the calculation was successful, let's also calculate the whole integer result
            let i64_result = Context::<i64>::default().evaluate(formula)?;

            // our subminutes are three decimal places, so we'll convert those to whole numbers, discarding the rest
            let f64_multiplied = (f64_result * 1000.0).floor() as i64;

            // now lets check if the original results were the same (we need the multiplication, otherwise things like
            // .305 == 0 would be true)
            if f64_multiplied == 1000 * i64_result {
                Ok(i64_result)
            } else if f64_multiplied < 100_000 {
                // if we only have 5 total orders, its fine to return the converted decimal number
                Ok(f64_multiplied)
            } else {
                // otherwise, just return the whole number result
                Ok(i64_result)
            }
        },
        Err(CalcError::Parse(_)) if formula.chars().any(|cc| cc == '(' || cc == ')') => {
            // TODO: Try to evaluate each bracket pair on their own, then concat the results, and
            // evaluate the result again..
            evaluate_with_each_bracket_separately(formula)
        }
        // if that calculation failed, let's try again with whole numbers and just return that result
        // TODO: We could probably optimize this to directly return parsing errors.
        Err(_) => Context::<i64>::default().evaluate(formula).map_err(Into::into),
    }
}

#[derive(Debug, Eq, PartialEq)]
enum FormulaPart {
    Normal(String),
    Bracket(Vec<FormulaPart>),
}

impl FormulaPart {
    fn evaluate_and_substitute_brackets(&self) -> Result<String, EvaluationError> {
        match self {
            Self::Normal(string) => Ok(string.clone()),
            Self::Bracket(parts) => {
                let mut result = String::new();

                for part in parts {
                    result.push_str(&part.evaluate_and_substitute_brackets()?);
                }

                evaluate(&result).map(|result|format!("{}", result))
            }
        }
    }
}

/// Takes a formula, finds pairs of brackets `(...)`, evaluates them separately, replaces them with the evaluation
/// result, then runs a final evaluation step at the end. This is intended for cases where the formula is given in
/// a form like this: N 48° 42.(A+B)(C+D)(E-F)
fn evaluate_with_each_bracket_separately(formula: &str) -> Result<i64, EvaluationError> {
    let count_opening_brackets = formula.chars().filter(|&cc| cc == '(').count();
    let count_closing_brackets = formula.chars().filter(|&cc| cc == ')').count();

    let formula = if count_opening_brackets == count_closing_brackets {
        // good!
        split_formula(&mut formula.chars())
    } else if count_opening_brackets == count_closing_brackets + 1 {
        // lets assume a closing bracket at the end
        split_formula(&mut formula.chars().chain(std::iter::once(')')))
    } else {
        return Err(EvaluationError::Syntax("Mismatch in number of opening and closing brackets".to_string()));
    };

    let formula = FormulaPart::Bracket(formula).evaluate_and_substitute_brackets()?;
    evaluate(&formula)
}

fn split_formula<CharIter>(formula_chars: &mut CharIter) -> Vec<FormulaPart>
where
    CharIter: Iterator<Item = char>
{
    let mut result = Vec::new();
    let mut outer_string = String::new();

    while let Some(cc) = formula_chars.next() {
        match cc {
            '(' => {
                if !outer_string.is_empty() {
                    result.push(FormulaPart::Normal(outer_string));
                    outer_string = String::new();
                }
                result.push(FormulaPart::Bracket(split_formula(formula_chars)));
            },
            ')' => {
                if !outer_string.is_empty() {
                    result.push(FormulaPart::Normal(outer_string));
                }
                return result;
            },
            other => {
                outer_string.push(other);
            }
        }
    }

    if !outer_string.is_empty() {
        result.push(FormulaPart::Normal(outer_string));
    }

    result
}


#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::{assert_eq};

    #[test]
    fn formula_f64_result() {
        let data = [
            // make sure that decimal places dont screw our final results
            ("((2*1500) - 2695) / 1000", 305),
            ("((2*1500) - 852) / 1000", 2148),
            ("((2*15000) - 2695) / 1000", 27305),
            ("((2*15000) - 852) / 1000", 29148),
            // these /1000 should be ignored because they are commonly used by geocachers to indicate that we need a .
            ("12345 / 1000", 12345),
            ("9042 / 1000", 9042),
            // however, normal calculations should not be discarded
            ("12345 / 5", 2469),
        ];

        for (formula, expected) in data {
            let result = evaluate(formula);
            assert!(result.is_ok(), "Calculation for formula \"{}\" returned an error: {:?}", formula, result);
            assert_eq!(expected, result.unwrap(), "Failed for formula {}", formula);
        }
    }

    #[test]
    fn formula_i64_result() {
        let data = [
            ("((2*1500) - 1000) / 1000", 2),
            ("((2*1500) - 1000) / 10", 200),
            ("17000", 17000)
        ];

        for (formula, expected) in data {
            let result = evaluate(formula);
            assert!(result.is_ok(), "Calculation for formula \"{}\" returned an error: {:?}", formula, result);
            assert_eq!(expected, result.unwrap(), "Failed for formula {}", formula);
        }
    }

    #[test]
    fn split_formula_typical() {
        use super::FormulaPart as FP;

        let formula = "12.(A+B)(C+D)(E-F)";
        let expected = vec![
            FP::Normal("12.".to_string()),
            FP::Bracket(vec![FP::Normal("A+B".to_string())]),
            FP::Bracket(vec![FP::Normal("C+D".to_string())]),
            FP::Bracket(vec![FP::Normal("E-F".to_string())]),
        ];

        let result = split_formula(&mut formula.chars());
        assert_eq!(expected, result, "Failed for formula {}", formula);
    }

    #[test]
    fn split_formula_brackets_in_between() {
        use super::FormulaPart as FP;

        let formula = "12.A(B+C)D";
        let expected = vec![
            FP::Normal("12.A".to_string()),
            FP::Bracket(vec![FP::Normal("B+C".to_string())]),
            FP::Normal("D".to_string()),
        ];

        let result = split_formula(&mut formula.chars());
        assert_eq!(expected, result, "Failed for formula {}", formula);
    }

    #[test]
    fn split_formula_nested() {
        use super::FormulaPart as FP;

        let formula = "12.(A+(B*C)+D)E(F*(G*(H-I)))";
        let expected = vec![
            FP::Normal("12.".to_string()),
            FP::Bracket(vec![
                FP::Normal("A+".to_string()),
                FP::Bracket(vec![
                    FP::Normal("B*C".to_string()),
                ]),
                FP::Normal("+D".to_string()),
            ]),
            FP::Normal("E".to_string()),
            FP::Bracket(vec![
                FP::Normal("F*".to_string()),
                FP::Bracket(vec![
                    FP::Normal("G*".to_string()),
                    FP::Bracket(vec![
                        FP::Normal("H-I".to_string()),
                    ]),
                ]),
            ]),
        ];

        let result = split_formula(&mut formula.chars());
        assert_eq!(expected, result, "Failed for formula {}", formula);
    }
}
