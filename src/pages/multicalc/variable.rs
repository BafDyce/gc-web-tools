use std::{collections::VecDeque, iter::IntoIterator};

use crate::{
    util::NeqAssign,
    sanitize::{Sanitize, FormatError},
};
use super::{calcwrapper, value::Value};

use lazy_static::lazy_static;
use regex::Regex;
use serde::{Serialize, Deserialize};


#[derive(Clone, Debug, Default, Deserialize, PartialEq, Serialize)]
pub struct Variable {
    name: String,
    answer: String,
    calc_method: VariableCalculationMethod,
}

#[derive(Clone, Copy, Debug, Deserialize, PartialEq, Serialize)]
pub enum VariableCalculationMethod {
    Raw,
    SumOfDigits,
    IteratedSumOfDigits,
    NumberOfDigits,
    Formula,
}

/// What kind of variable this is. This information is necessary, e.g. to know whether it should be used for the
/// checksum check.
#[derive(Clone, Copy, Debug, Deserialize, PartialEq, Serialize)]
pub enum VariableKind {
    /// Normal, user-controlled variable
    Variable,
    /// Variable of a stage (N0, S1, etc.)
    Stage,
    /// Special type which holds the value of the sum of all variables with type `Self::Variable`
    Sum,
}

impl Variable {
    pub fn answer(&self) -> &str {
        &self.answer
    }

    pub fn calc_method(&self) -> VariableCalculationMethod {
        self.calc_method
    }

    pub fn is_formula(&self) -> bool {
        self.calc_method == VariableCalculationMethod::Formula
    }

    pub fn has_name(&self) -> bool {
        !self.name.trim().is_empty()
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn name_matches_reserved_pattern(name: &str) -> bool {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"^([NSEW]{1}\d+|SUM)$").unwrap();
        }

        RE.is_match(name)
    }

    pub fn set_answer(&mut self, answer: String) -> bool {
        self.answer.neq_assign(answer)
    }

    pub fn set_calc_method(&mut self, calc_method: VariableCalculationMethod) -> bool {
        self.calc_method.neq_assign(calc_method)
    }

    pub fn set_name(&mut self, name: String) -> (bool, Vec<FormatError>) {
        let (sanitized_name, errors) = Self::sanitized_name(&name);
        let errors = errors.into_iter().map(|(_, error)| error).collect();

        let changed = self.name.neq_assign(sanitized_name);
        (changed, errors)
    }

    pub fn value<S: AsRef<str>>(&self, variables: &VecDeque<(S, Value<i32>, VariableKind)>) -> Result<Value<i32>, String> {
        self.value_with_calc_method(variables, self.calc_method)
    }

    pub fn value_with_calc_method<S: AsRef<str>>(
        &self,
        variables: &VecDeque<(S, Value<i32>, VariableKind)>,
        calc_method: VariableCalculationMethod,
    ) -> Result<Value<i32>, String> {
        if self.answer.trim().is_empty() {
            return Ok(Value::default());
        }

        match calc_method {
            VariableCalculationMethod::Raw => {
                self.answer.parse::<i32>().map(Value::new).map_err(|err| format!("Failed to parse: {}", err))
            },
            VariableCalculationMethod::SumOfDigits => Ok(Value::new(Self::sum_of_digits(&self.answer))),
            VariableCalculationMethod::IteratedSumOfDigits => {
                let mut sum = Self::sum_of_digits(&self.answer);

                while sum >= 10 {
                    sum = Self::sum_of_digits(&format!("{}", sum));
                }

                Ok(Value::new(sum))
            }
            VariableCalculationMethod::NumberOfDigits => Ok(Value::new(self.answer.trim().len() as i32)),
            VariableCalculationMethod::Formula => {
                let mut formula: String = self.answer.chars().skip(1).collect();
                let mut info = None;

                for (varname, value, _) in variables {
                    if formula.contains(varname.as_ref()) && !value.is_value_set() {
                        info = Some("Uses variable which doesn't exist or cannot be calculated".to_string());
                    }
                    formula = formula.replace(varname.as_ref(), &value.value().to_string());
                }

                match calcwrapper::evaluate(&formula) {
                    Ok(result) => Ok(Value::new(result as i32).with_info(info)),
                    Err(err) => Err(format!("Failed to calculate variable: {}", err)),
                }
            }
        }
    }

    fn sum_of_digits(input: &str) -> i32 {
        let mut sum = 0;

        for cc in input.chars() {
            match cc {
                '0' => sum += 0,
                '1' => sum += 1,
                '2' => sum += 2,
                '3' => sum += 3,
                '4' => sum += 4,
                '5' => sum += 5,
                '6' => sum += 6,
                '7' => sum += 7,
                '8' => sum += 8,
                '9' => sum += 9,
                'a' | 'A' => sum += 1,
                'b' | 'B' => sum += 2,
                'c' | 'C' => sum += 3,
                'd' | 'D' => sum += 4,
                'e' | 'E' => sum += 5,
                'f' | 'F' => sum += 6,
                'g' | 'G' => sum += 7,
                'h' | 'H' => sum += 8,
                'i' | 'I' => sum += 9,
                'j' | 'J' => sum += 10,
                'k' | 'K' => sum += 11,
                'l' | 'L' => sum += 12,
                'm' | 'M' => sum += 13,
                'n' | 'N' => sum += 14,
                'o' | 'O' => sum += 15,
                'p' | 'P' => sum += 16,
                'q' | 'Q' => sum += 17,
                'r' | 'R' => sum += 18,
                's' | 'S' => sum += 19,
                't' | 'T' => sum += 20,
                'u' | 'U' => sum += 21,
                'v' | 'V' => sum += 22,
                'w' | 'W' => sum += 23,
                'x' | 'X' => sum += 24,
                'y' | 'Y' => sum += 25,
                'z' | 'Z' => sum += 26,
                _ => {}
            }
        }

        sum
    }

    /// Internal helper method for implementation of Sanitize trait
    fn sanitized_name(name: &str) -> (String, Vec<(&'static str, FormatError)>) {
        let mut only_alphanumeric_allowed_triggered = false;
        let mut letter_seen = false;
        let mut must_start_with_letter_triggered = false;

        let sanitized_name = name.chars().filter_map(|cc| {
            match cc {
                '0' ..= '9' => {
                    if !letter_seen {
                        must_start_with_letter_triggered = true;
                        None
                    } else {
                        Some(cc)
                    }
                }
                'a' ..= 'z' => {
                    letter_seen = true;
                    Some(cc.to_ascii_uppercase())
                },
                'A' ..= 'Z' => {
                    letter_seen = true;
                    Some(cc)
                },
                _ => {
                    only_alphanumeric_allowed_triggered = true;
                    None
                },
            }
        }).collect();

        let mut errors = Vec::new();
        if only_alphanumeric_allowed_triggered {
            errors.push(("name", FormatError::OnlyAlphaNumericAllowed));
        }
        if must_start_with_letter_triggered {
            errors.push(("name", FormatError::MustStartWithLetter));
        }

        (sanitized_name, errors)
    }
}

impl Default for VariableCalculationMethod {
    fn default() -> Self {
        Self::Raw
    }
}

#[cfg(test)]
impl From<(&str, &str, VariableCalculationMethod)> for Variable {
    fn from((name, answer, calc_method): (&str, &str, VariableCalculationMethod)) -> Self {
        Self {
            name: name.to_string(),
            answer: answer.to_string(),
            calc_method,
        }.sanitized().0
    }
}

impl Sanitize for Variable {
    fn sanitized(mut self) -> (Self, Vec<(&'static str, FormatError)>) {
        let (sanitized_name, errors) = Self::sanitized_name(&self.name);

        self.name = sanitized_name;
        (self, errors)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn variable_sanitized_name() {
        let data = [
            ("A", "A", vec![]),
            ("a", "A", vec![]),
            ("A1", "A1", vec![]),
            ("1A", "A", vec![FormatError::MustStartWithLetter]),
            ("a b", "AB", vec![FormatError::OnlyAlphaNumericAllowed]),
            ("!@#$%^&*()|\"\'\\", "", vec![FormatError::OnlyAlphaNumericAllowed]),
            ("!@#$%^&*1()|\"\'\\", "", vec![FormatError::OnlyAlphaNumericAllowed, FormatError::MustStartWithLetter]),
        ];

        for (input, expected_sanitized_name, expected_errors) in data {
            let (sanitized_name, errors) = Variable::sanitized_name(input);
            let expected_errors: Vec<_> = expected_errors.into_iter().map(|error| ("name", error)).collect();

            assert_eq!(expected_sanitized_name, sanitized_name, "Wrong sanitized name for {}", input);
            assert_eq!(expected_errors, errors, "Wrong errors for {}", input);
        }
    }

    #[test]
    fn name_matches_reserved_pattern() {
        let data = [
            // direction letters without digit
            ("E", false),
            ("N", false),
            ("S", false),
            ("W", false),

            // valid stage variable names
            ("E0", true),
            ("N1", true),
            ("S2", true),
            ("W3", true),
            ("E12", true),
            ("N345", true),
            ("S6789", true),

            // letter between direction and digit
            ("WX0", false),

            // valid patter as part of name -> should not match!
            ("AE0", false),
            ("BN1", false),
            ("CS2", false),
            ("DW3", false),
            ("FE12", false),
            ("GN345", false),
            ("HS6789", false),

            // double letters at the beginning
            ("EE0", false),
            ("NN1", false),
            ("SS2", false),
            ("WW3", false),
            ("EE12", false),
            ("NN345", false),
            ("SS6789", false),

            // same as abover but other valid direction char
            ("EW0", false),
            ("NS1", false),
            ("SN2", false),
            ("WE3", false),
            ("EW12", false),
            ("NE345", false),
            ("SE6789", false),

            // valid pattern but followed by a letter
            ("E0A", false),
            ("N1B", false),
            ("S2C", false),
            ("W3D", false),
            ("E12E", false),
            ("N345S", false),
            ("S6789N", false),

            // other special variables
            ("SUM", true),
            ("SUMMER", false),
        ];

        for (name, expected) in data {
            let result = Variable::name_matches_reserved_pattern(name);

            assert_eq!(expected, result, "Failed for variable name: {}", name);
        }
    }
}
