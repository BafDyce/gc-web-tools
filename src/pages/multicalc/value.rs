/// Container for values which optionally can have additional information attached
#[derive(Clone, Debug, Default, PartialEq)]
pub struct Value<T: Copy + Default> {
    value: Option<T>,
    info: Option<String>,
}

impl<T: Copy + Default> Value<T> {
    /// Crates a new Value wrapper around the given value. If you need a `Value` with `value = None`, then use
    /// `Value::default()` or `Value::new_with()`
    pub fn new(value: T) -> Self {
        Self::new_with(Some(value))
    }

    pub fn new_with(value: Option<T>) -> Self {
        Self {
            value,
            .. Self::default()
        }
    }

    pub fn default_with_info(info: String) -> Self {
        Self {
            info: Some(info),
            .. Self::default()
        }
    }

    pub fn info(&self) -> &Option<String> {
        &self.info
    }

    pub fn is_value_set(&self) -> bool {
        self.value.is_some()
    }

    pub fn with_info(mut self, info: Option<String>) -> Self {
        self.info = info;
        self
    }

    /// Returns the contained value or the value's default
    pub fn value(&self) -> T {
        self.value.unwrap_or_default()
    }
}
