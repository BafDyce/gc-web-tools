use crate::{
    icon::{Icon, IconSvg},
    sanitize::{FormatError, Sanitize},
    storage::{LocalStorage, StorageItem},
    util::{copy_to_clipboard, GeoCode, NeqAssign},
};
use coord::{CalculatedCoordinate, CoordinateError, CoordinatePart, CoordinatePartInner};
use std::collections::{HashMap, HashSet, VecDeque};

use chrono::{DateTime, Duration, Utc};
use gloo_console::error;
use gloo_timers::{callback::Timeout, future::TimeoutFuture};
use serde::{Deserialize, Serialize};
use wasm_bindgen::{JsCast, JsValue};
use wasm_bindgen_futures::spawn_local;
use web_sys::{HtmlInputElement, HtmlSelectElement};
use yew::{
    classes,
    events::{Event, InputEvent, MouseEvent, PointerEvent, TargetCast},
    html,
    Component, Context, Html, Properties,
};

mod calcwrapper;
mod checksum;
mod coord;
mod urldata;
mod value;
mod variable;

use self::{
    checksum::{Checksum, ChecksumResult},
    coord::{Direction, Latitude, Longitude},
    urldata::Urldata,
    value::Value,
    variable::{Variable, VariableCalculationMethod, VariableKind},
};

pub struct MultiCalcPage {
    data: MultiCalc,
    stage_coords_copied: Option<(usize, usize, bool)>,
    stage_onpointerdown_data: Option<(usize, usize, DateTime<Utc>)>,
    variable_errors: Vec<FormatError>,
    old_checksum: Checksum,
    notes_copied: Option<(usize, bool)>,
}

pub enum MultiCalcMsg {
    /// cache_idx
    AddStage(usize),
    AddVariable(usize),
    ChecksumSetMethodSum(usize),
    ChecksumSetMethodFormula(usize),
    ChecksumUpdateSumValue(usize, String),
    ChecksumUpdateFormulaValueLeft(usize, String),
    ChecksumUpdateFormulaValueRight(usize, String),
    ChecksumToggle(usize),
    DeleteStage(usize, usize),
    DeleteVariable(usize, usize),
    HashUpdated,
    OpenMenu(OpenedMenu),
    PlaintextNotesCopied(usize, bool),
    PlaintextNotesCopiedReset,
    Reset,
    StageCoordsCopied(usize, usize, bool),
    StageCoordsCopiedReset,
    StagePointerDown(usize, usize),
    StagePointerUp(usize, usize),
    UpdateCacheCode(usize, String),
    UpdateCacheName(usize, String),
    UpdateStageName(usize, usize, String),
    UpdateStageLatDirection(usize, usize, Direction),
    UpdateStageLatDegrees(usize, usize, String),
    UpdateStageLatMinutes(usize, usize, String),
    UpdateStageLonDirection(usize, usize, Direction),
    UpdateStageLonDegrees(usize, usize, String),
    UpdateStageLonMinutes(usize, usize, String),
    UpdateVariableAnswer(usize, usize, String),
    UpdateVariableCalculationMethod(usize, usize, VariableCalculationMethod),
    UpdateVariableName(usize, usize, String),
}

#[derive(Clone, Debug, Default, PartialEq, Properties)]
pub struct MultiCalcProps {
    pub data: MultiCalc,
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct MultiCalc {
    caches: Vec<MultiCache>,
    opened_menu: OpenedMenu,
}

/// Does not support `Deserialize` and `Serialize` on purpose. Use `Urldata::from()` amd
/// `Self::from(Urldata)` instead
#[derive(Clone, Debug, PartialEq)]
pub(self) struct MultiCache {
    id: GeoCode,
    name: String,
    variables: Vec<Variable>,
    stages: Vec<Stage>,
    checksum: Option<Checksum>,
}

#[derive(Clone, Debug, Default, Deserialize, PartialEq, Serialize)]
struct Stage {
    name: String,
    lat: Latitude,
    lon: Longitude,
}

#[derive(Clone, Debug, PartialEq)]
pub enum OpenedMenu {
    /// No menu is opened
    None,
    /// Menu to change checksum is opened
    Checksum,
    /// Menu to change gc code and name is opened
    GcDetails,
    /// Stage with the given index is opened
    Stage(usize),
    /// Variable with the given index is opened
    Variable(usize),
}

#[derive(Clone, Debug, PartialEq)]
struct MultiCacheCalcResult<'a> {
    /// References to each variable + its calculated value (or an error)
    variables: Vec<(&'a Variable, Result<Value<i32>, String>)>,
    /// References to each stage + its calculated value (or an error)
    stages: Vec<(
        &'a Stage,
        Result<Value<CalculatedCoordinate>, CoordinateError>,
    )>,
    /// List of all variable names and their calculation results (0 if it was an error)
    /// This also includes Lat/Lon variables of the stages
    all_variables_list: VecDeque<(String, Value<i32>, VariableKind)>,
    /// Whether the checksum passes
    checksum: Option<ChecksumResult>,
}

impl MultiCalc {
    pub fn load() -> Self {
        let multi = web_sys::window()
            .map(|window| match window.location().hash() {
                Ok(hash) if hash.is_empty() => MultiCache::default(),
                Ok(hash) => match urlencoding::decode(&hash[1..]) {
                    Ok(json) => match Urldata::multicache_from_json(&json) {
                        Ok(multicache) => {
                            if let Some(last_stage) = multicache.stages.last() {
                                let storage = LocalStorage::new();
                                storage.set(StorageItem::LatitudeDirection, last_stage.lat.direction());
                                storage.set(StorageItem::LatitudeDegrees, last_stage.lat.degrees());
                                storage.set(StorageItem::LongitudeDirection, last_stage.lon.direction());
                                storage.set(StorageItem::LongitudeDegrees, last_stage.lon.degrees());
                            }

                            multicache
                        },
                        Err(err) => {
                            error!("text",
                                JsValue::from(format!(
                                    "Failed to parse url data: {:?} ({})",
                                    err, hash
                                ))
                            );
                            MultiCache::default()
                        }
                    },
                    Err(err) => {
                        error!("text", JsValue::from(format!("Failed to decode hash: {:?}", err)));
                        MultiCache::default()
                    }
                },
                Err(err) => {
                    error!("text", JsValue::from(format!("Failed to access hash: {:?}", err)));
                    MultiCache::default()
                }
            })
            .unwrap_or_default();

        Self {
            caches: vec![multi],
            opened_menu: OpenedMenu::None,
        }
    }
}

impl MultiCalcPage {
    fn draw_cache(&self, ctx: &Context<Self>, idx: usize) -> Html {
        let cache = &self.data.caches[idx];
        let cache_result = cache.calc_all();

        let duplicate_variables: HashSet<String> = {
            let mut map: HashMap<String, usize> = HashMap::new();

            for (var, _) in &cache_result.variables {
                let entry = map.entry(var.name().to_owned()).or_default();
                *entry += 1;
            }

            map.into_iter()
                .filter_map(
                    |(varname, count)| {
                        if count > 1 {
                            Some(varname)
                        } else {
                            None
                        }
                    },
                )
                .collect()
        };

        let onclick_stop_propagation = ctx.link().batch_callback(|ee: MouseEvent| {
            ee.stop_propagation();
            None
        });
        let onclick_change_gcdetails = ctx.link().callback(|ee: MouseEvent| {
            ee.stop_propagation();
            MultiCalcMsg::OpenMenu(OpenedMenu::GcDetails)
        });
        // Data is auto-saved as soon as user enters something. The user could also exit the form by
        // just clicking anywhere but it's probably more intuitive (and sometimes easier) to have a
        // dedicated button for it. When clicking it, we'll just close the current menu.
        let onclick_fake_submit = ctx.link().callback(|ee: MouseEvent| {
            ee.stop_propagation();
            MultiCalcMsg::OpenMenu(OpenedMenu::None)
        });
        let oninput_geocode = ctx
            .link()
            .batch_callback(move |ee: InputEvent| {
                ee.target_dyn_into::<HtmlInputElement>().map(|input| MultiCalcMsg::UpdateCacheCode(idx, input.value()))
            });

        let oninput_gcname = ctx
            .link()
            .batch_callback(move |ee: InputEvent| {
                ee.target_dyn_into::<HtmlInputElement>().map(|input| MultiCalcMsg::UpdateCacheName(idx, input.value()))
            });

        let geocode = cache.id.as_str().to_string();
        let plaintext_note = cache.plaintext_note(&cache_result);
        let plaintext_note_copy = plaintext_note.clone();

        html! {
            <div class="container" style="max-width: 95%">
                <h2 class="text-center clickable">
                    { if self.data.opened_menu == OpenedMenu::GcDetails {
                        let reset_modal_id = "cache-reset-modal".to_string();
                        let reset_modal_id_label = "cache-reset-modal-label".to_string();
                        let onclick_reset = ctx.link().callback(|_: MouseEvent| {
                            MultiCalcMsg::Reset
                        });

                        html! {
                            <>
                            <div class="input-group" onclick = {onclick_stop_propagation.clone()} >
                                <span class="input-group-text" style="flex: none; width: 3rem;">{"GC"}</span>

                                <input
                                    type = "text"
                                    placeholder = "3XMPL"
                                    value = {geocode}
                                    class = "form-control"
                                    style = "flex: none; width: 7rem;"
                                    oninput = {oninput_geocode}
                                />

                                <span class="input-group-text" style="flex: none; width: 4rem;">{"Name"}</span>

                                <input
                                    type = "text"
                                    placeholder = "Example geocache"
                                    value = {cache.name.clone()}
                                    class = "form-control"
                                    oninput = {oninput_gcname}
                                />

                                <button
                                    type = "button"
                                    class = "btn btn-success float-end"
                                    onclick = {onclick_fake_submit}
                                >{"Submit"}</button>
                                <button
                                    type = "button"
                                    class = "btn btn-outline-danger float-end"
                                    data-bs-toggle = "modal"
                                    data-bs-target = {format!("#{}", reset_modal_id)}
                                >{"Reset"}</button>
                            </div>

                            // Modal for resetting
                            <div
                                id = {reset_modal_id}
                                class = "modal fade"
                                tabindex = "-1"
                                aria-labeledby = {reset_modal_id_label.clone()}
                                aria-hidden = "true"
                            >
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 class="modal-title" id={reset_modal_id_label}>{"Reset cache?"}</h3>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <p class="fs-4">
                                                {format!("Do you really want to reset the cache {} - {}?", cache.id, cache.name)}
                                                <br/>
                                                <strong>{"This will also delete all variables and stages!"}</strong>
                                                <br/>
                                                {"If you want to create an additional tab for another multi, use the button below"}
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{"Cancel"}</button>

                                            <a
                                                class="btn btn-primary"
                                                href="/"
                                                target="_blank"
                                                onclick={onclick_stop_propagation.clone()}
                                                data-bs-dismiss="modal"
                                            >{"Empty cache in new tab"}</a>

                                            <button
                                                type = "button"
                                                class = "btn btn-danger"
                                                onclick = {onclick_reset}
                                                data-bs-dismiss="modal"
                                                onclick={onclick_stop_propagation.clone()}
                                            >{"Reset cache!"}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </>
                        }
                    } else {
                        html!{
                            <>
                                <span onclick = {onclick_change_gcdetails.clone()} >
                                    <IconSvg icon = {Icon::Pencil} height=".75em" />
                                    {format!(" {}", cache.id)}
                                </span>
                                <a
                                    class = "text-decoration-none"
                                    href = {cache.id.url()}
                                    target = "_blank"
                                    rel = "noreferrer noopener"
                                >
                                    <IconSvg
                                        icon = {Icon::BoxArrowUpRight}
                                        height = "20"
                                        additional_classes = {vec!["align-top"]}
                                    />
                                </a>
                                <span onclick = {onclick_change_gcdetails}>
                                    {format!(" - {} ", cache.name)}
                                    <IconSvg icon = {Icon::Pencil} height=".75em" />
                                </span>
                            </>
                        }
                    }}
                </h2>

                <h3>{format!("Variables (sum = {})", cache_result.get_sum().value())}</h3>
                <div class="d-flex flex-wrap p-2">
                    {self.draw_checksum(ctx, cache, idx, &cache_result)}

                    <button
                        type="button"
                        class="multi-variable border-2 rounded-3 btn btn-lg btn-primary"
                        onclick={ctx.link().callback(move |ee: MouseEvent| {
                            ee.stop_propagation();
                            MultiCalcMsg::AddVariable(idx)
                        })}
                    >
                        {"Add variable"}
                    </button>

                    {for cache_result.variables.iter().enumerate().map(|(vidx, (var, value))| {
                        let variable_onclick = ctx.link().callback(move |ee: MouseEvent| {
                            ee.stop_propagation();
                            MultiCalcMsg::OpenMenu(OpenedMenu::Variable(vidx))
                        });

                        let (calculated_value, css_class_bg_warning) = match value {
                            Ok(result) if result.info().is_some() || !result.is_value_set() => {
                                ("[Err!]".to_string(), Some("bg-warning"))
                            }
                            Ok(result) => (result.value().to_string(), None),
                            Err(_) => ("[Err!]".to_string(), Some("bg-warning")),
                        };

                        let css_class_bg_warning = css_class_bg_warning.or_else(|| {
                            if duplicate_variables.contains(var.name())
                            || Variable::name_matches_reserved_pattern(var.name()) {
                                Some("bg-warning")
                            } else {
                                None
                            }
                        });

                        if self.data.opened_menu == OpenedMenu::Variable(vidx) {
                            let oninput_name = ctx.link().batch_callback(move |ee: InputEvent| {
                                ee.target_dyn_into::<HtmlInputElement>()
                                    .map(|input| MultiCalcMsg::UpdateVariableName(idx, vidx, input.value()))
                            });
                            let oninput_answer = ctx.link().batch_callback(move |ee: InputEvent| {
                                ee.target_dyn_into::<HtmlInputElement>()
                                .map(|input| MultiCalcMsg::UpdateVariableAnswer(idx, vidx, input.value()))
                            });
                            let is_formula = var.is_formula();

                            let onclick_calculation_method = |method: VariableCalculationMethod| -> yew::Callback<MouseEvent> {
                                ctx.link().batch_callback(move |_| {
                                    if is_formula {
                                        None
                                    } else {
                                        Some(MultiCalcMsg::UpdateVariableCalculationMethod(idx, vidx, method))
                                    }
                                })
                            };

                            let css_id_name_input_label = format!("variable-{}-name", vidx);
                            let css_id_answer_input_label = format!("variable-{}-answer", vidx);
                            let css_id_value_input_label = format!("variable-{}-value", vidx);

                            let css_id_input_label_calc_raw = format!("variable-{}-calc-raw", vidx);
                            let css_id_input_label_calc_sum_of_digits = format!("variable-{}-calc-sumofdigits", vidx);
                            let css_id_input_label_calc_sum_of_digits_iterated = format!("variable-{}-calc-iteratedsumofdigits", vidx);
                            let css_id_input_label_calc_number_of_digits = format!("variable-{}-calc-numberofdigits", vidx);

                            let delete_modal_id = format!("variable-{}-delete-modal", vidx);
                            let delete_modal_id_label = format!("variable-{}-delete-modal-label", vidx);
                            let onclick_stage_delete = ctx.link().callback(move |ee: MouseEvent| {
                                ee.stop_propagation();
                                MultiCalcMsg::DeleteVariable(idx, vidx)
                            });
                            let onclick_fake_submit = ctx.link().callback(|ee: MouseEvent| {
                                ee.stop_propagation();
                                MultiCalcMsg::OpenMenu(OpenedMenu::None)
                            });

                            html!{
                                <div class="multi-variable border-2 rounded-3" onclick = {variable_onclick} >
                                    <div class="row mb-1">
                                        <div class="col-12">
                                            <div class="input-group">
                                                <span
                                                    class="input-group-text"
                                                    id = {css_id_name_input_label.clone()}
                                                    style = "max-width: 5rem; padding: 0.2rem;"
                                                >{"Name"}</span>
                                                <input
                                                    type="text"
                                                    class="form-control"
                                                    placeholder = "e.g., A"
                                                    value = {var.name().to_owned()}
                                                    aria-label = "Variable name"
                                                    aria-described-by = {css_id_name_input_label}
                                                    oninput = {oninput_name}
                                                    onclick = {ctx.link().batch_callback(|ee: MouseEvent| {
                                                        ee.stop_propagation();
                                                        None
                                                    })}
                                                    style = "max-width: 3.5rem;"
                                                />

                                                <span
                                                    class="input-group-text"
                                                    id={css_id_answer_input_label.clone()}
                                                    style = "max-width: 5rem; padding: 0.2rem;"
                                                >{"Answer"}</span>
                                                <input
                                                    type="text"
                                                    class="form-control"
                                                    placeholder = "e.g., 42, geocaching, =A+B"
                                                    value = {var.answer().to_owned()}
                                                    aria-label = "Variable answer"
                                                    aria-described-by = {css_id_answer_input_label}
                                                    oninput = {oninput_answer}
                                                    onclick = {ctx.link().batch_callback(|ee: MouseEvent| {
                                                        ee.stop_propagation();
                                                        None
                                                    })}
                                                />

                                                <span
                                                    class="input-group-text"
                                                    id={css_id_value_input_label.clone()}
                                                    style = "max-width: 2rem; padding: 0;"
                                                >{"="}</span>
                                                <input
                                                    type="text"
                                                    class={classes!("form-control", "text-center", css_class_bg_warning)}
                                                    value = {calculated_value}
                                                    disabled = {true}
                                                    readonly = {true}
                                                    aria-label = "Variable value"
                                                    aria-described-by = {css_id_value_input_label}
                                                    style = "max-width: 4rem; padding: 0;"
                                                />

                                                <button
                                                    type = "button"
                                                    class = "btn btn-success float-end"
                                                    style = "width: 4rem; padding: 0;"
                                                    onclick = {onclick_fake_submit}
                                                >{"Submit"}</button>
                                                <button
                                                    type = "button"
                                                    class = "btn btn-danger"
                                                    style = "flex: none; width: 3.5rem; padding: 0;"
                                                    data-bs-toggle = "modal"
                                                    data-bs-target = {format!("#{}", delete_modal_id)}
                                                >{"Delete"}</button>

                                                // Modal for deletion dialog
                                                <div
                                                    id = {delete_modal_id}
                                                    class = "modal fade"
                                                    tabindex = "-1"
                                                    aria-labeledby = {delete_modal_id_label.clone()}
                                                    aria-hidden = "true"
                                                >
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id={delete_modal_id_label}>
                                                                    {format!("Do you really wanna delete variable {}?", var.name())}
                                                                </h5>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{"Cancel"}</button>
                                                                <button
                                                                    type = "button"
                                                                    class = "btn btn-danger"
                                                                    onclick = {onclick_stage_delete}
                                                                    data-bs-dismiss="modal"
                                                                >{"Yes, delete it!"}</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    // calculation method selection
                                    <div class="row">
                                        <div class="input-group">
                                            <div class="btn-group" role="group" style = "flex: max-content">
                                                <input
                                                    type = "radio"
                                                    class = "btn-check"
                                                    name = "TODO"
                                                    id = {css_id_input_label_calc_raw.clone()}
                                                    autocomplete = "off"
                                                    disabled = {is_formula}
                                                    checked = {VariableCalculationMethod::Raw == var.calc_method()}
                                                    onclick = {onclick_calculation_method(VariableCalculationMethod::Raw)}
                                                />
                                                <label
                                                    class = "btn btn-outline-primary"
                                                    for = {css_id_input_label_calc_raw}
                                                >{format!("As-is ({})",
                                                    var.value_with_calc_method(
                                                        &cache_result.all_variables_list,
                                                        VariableCalculationMethod::Raw
                                                    ).unwrap_or_default().value())
                                                }</label>

                                                <input
                                                    type = "radio"
                                                    class = "btn-check"
                                                    name = "TODO"
                                                    id = {css_id_input_label_calc_sum_of_digits.clone()}
                                                    autocomplete = "off"
                                                    disabled = {is_formula}
                                                    checked = {VariableCalculationMethod::SumOfDigits == var.calc_method()}
                                                    onclick = {onclick_calculation_method(VariableCalculationMethod::SumOfDigits)}
                                                />
                                                <label
                                                    class = "btn btn-outline-primary"
                                                    for = {css_id_input_label_calc_sum_of_digits}
                                                >{format!("Sum of digits ({})",
                                                    var.value_with_calc_method(
                                                        &cache_result.all_variables_list,
                                                        VariableCalculationMethod::SumOfDigits
                                                    ).unwrap_or_default().value())
                                                }</label>

                                                <input
                                                    type = "radio"
                                                    class = "btn-check"
                                                    name = "TODO"
                                                    id = {css_id_input_label_calc_sum_of_digits_iterated.clone()}
                                                    autocomplete = "off"
                                                    disabled = {is_formula}
                                                    checked = {VariableCalculationMethod::IteratedSumOfDigits == var.calc_method()}
                                                    onclick = {onclick_calculation_method(VariableCalculationMethod::IteratedSumOfDigits)}
                                                />
                                                <label
                                                    class = "btn btn-outline-primary"
                                                    for = {css_id_input_label_calc_sum_of_digits_iterated}
                                                >{format!("Iterated sum of digits ({})",
                                                    var.value_with_calc_method(
                                                        &cache_result.all_variables_list,
                                                        VariableCalculationMethod::IteratedSumOfDigits
                                                    ).unwrap_or_default().value())
                                                }</label>

                                                <input
                                                    type = "radio"
                                                    class = "btn-check"
                                                    name = "TODO"
                                                    id = {css_id_input_label_calc_number_of_digits.clone()}
                                                    autocomplete = "off"
                                                    disabled = {is_formula}
                                                    checked = {VariableCalculationMethod::NumberOfDigits == var.calc_method()}
                                                    onclick = {onclick_calculation_method(VariableCalculationMethod::NumberOfDigits)}
                                                />
                                                <label
                                                    class = "btn btn-outline-primary"
                                                    for = {css_id_input_label_calc_number_of_digits}
                                                >{format!("Number of digits ({})",
                                                    var.value_with_calc_method(
                                                        &cache_result.all_variables_list,
                                                        VariableCalculationMethod::NumberOfDigits
                                                    ).unwrap_or_default().value())
                                                }</label>
                                            </div>
                                        </div>
                                    </div>

                                    { match {
                                        if !var.has_name() {
                                            Some("Warning: Variable has no name!".to_string())
                                        } else if Variable::name_matches_reserved_pattern(var.name()) {
                                            Some("Warning: Variable name matches reserved name pattern! Will be ignored.".to_string())
                                        } else if duplicate_variables.contains(var.name()) {
                                            Some("Warning: Another variable with the same name exists!".to_string())
                                        } else if !self.variable_errors.is_empty() {
                                            Some(format!("Info: {:?}", self.variable_errors ))
                                        } else if value.is_ok() && value.as_ref().unwrap().info().is_some() {
                                            value.as_ref().unwrap().info().clone()
                                        } else if value.is_ok() && !value.as_ref().unwrap().is_value_set() {
                                            Some("Warning: No value set!".to_string())
                                        } else if value.is_err() {
                                            Some("Warning: Failed to calculate variable (wrong method or formula?)".to_string())
                                        } else {
                                            None
                                        }
                                    } {
                                        Some(message) => html!{
                                            <div class = "row mt-1"><div class="col-12">
                                            <div class = "border border-dark border rounded bg-warning bg-gradient text-dark fs-4">
                                                <span class="ps-2">
                                                    {message}
                                                </span>
                                            </div>
                                            </div></div>
                                        },
                                        None => html!{{""}},
                                    }}
                                </div>
                            }
                        } else {
                            let css_class_bg_warning = css_class_bg_warning.or_else(|| {
                                if !var.has_name() {
                                    Some("bg-warning")
                                } else {
                                    css_class_bg_warning
                                }
                            });

                            html!{
                                <div
                                    class = {classes!(
                                        "multi-variable", "border-2", "rounded-3", css_class_bg_warning,
                                    )}
                                    onclick = {variable_onclick}
                                >{format!("{} = {}", var.name(), calculated_value)}</div>
                            }
                        }
                    })}
                </div>

                <hr/>
                <h3>{"Stages"}</h3>
                <div class="container m-0" style="max-width: 100%;">
                    <div class="row justify-content-start">
                    {for cache_result.stages.iter().enumerate().map(|(sidx, (stage, value))| {
                        let (stage_coords, highlight_class, additional_info) = match value {
                            Ok(coords) => (
                                format!("{}", coords.value()),
                                if coords.info().is_some() { Some("bg-warning") } else { None },
                                coords.info().clone(),
                            ),
                            Err(err) => (format!("[Err] {:?}", err), Some("bg-warning"), None),
                        };

                        if self.data.opened_menu == OpenedMenu::Stage(sidx) {
                            // We have a global click handler that closes all menus (OpenMenu(None)) that spans across
                            // the entire page. Therefore, we need to put a click handler on our opened menu which
                            // prevents the global click handler from running.
                            let onclick = ctx.link().batch_callback(|ee: MouseEvent| {
                                ee.stop_propagation();
                                None
                            });

                            let oninput_name = ctx.link().batch_callback(move |ee: InputEvent| {
                                ee.target_dyn_into::<HtmlInputElement>()
                                    .map(|input| MultiCalcMsg::UpdateStageName(idx, sidx, input.value()))
                            });

                            let onchange_lat_direction = ctx.link().batch_callback(move |ee: Event| {
                                if let Some(input) = ee.target_dyn_into::<HtmlSelectElement>() {
                                    if let Ok(direction) = input.value().parse() {
                                        Some(MultiCalcMsg::UpdateStageLatDirection(idx, sidx, direction))
                                    } else {
                                        None
                                    }
                                } else {
                                    None
                                }
                            });
                            let onchange_lat_degrees = ctx.link().batch_callback(move |ee: Event| {
                                ee.target_dyn_into::<HtmlInputElement>()
                                    .map(|input| MultiCalcMsg::UpdateStageLatDegrees(idx, sidx, input.value()))
                            });
                            let oninput_lat_minutes = ctx.link().batch_callback(move |ee: InputEvent| {
                                ee.target_dyn_into::<HtmlInputElement>()
                                    .map(|input| MultiCalcMsg::UpdateStageLatMinutes(idx, sidx, input.value()))
                            });

                            let onchange_lon_direction = ctx.link().batch_callback(move |ee: Event| {
                                if let Some(input) = ee.target_dyn_into::<HtmlSelectElement>() {
                                    if let Ok(direction) = input.value().parse() {
                                        Some(MultiCalcMsg::UpdateStageLonDirection(idx, sidx, direction))
                                    } else {
                                        None
                                    }
                                } else {
                                    None
                                }
                            });
                            let onchange_lon_degrees = ctx.link().batch_callback(move |ee: Event| {
                                ee.target_dyn_into::<HtmlInputElement>()
                                    .map(|input| MultiCalcMsg::UpdateStageLonDegrees(idx, sidx, input.value()))
                            });
                            let oninput_lon_minutes = ctx.link().batch_callback(move |ee: InputEvent| {
                                ee.target_dyn_into::<HtmlInputElement>()
                                    .map(|input| MultiCalcMsg::UpdateStageLonMinutes(idx, sidx, input.value()))
                            });

                            let delete_modal_id = format!("stage-{}-delete-modal", sidx);
                            let delete_modal_id_label = format!("stage-{}-delete-modal-label", sidx);
                            let onclick_stage_delete = ctx.link().callback(move |_: MouseEvent| {
                                MultiCalcMsg::DeleteStage(idx, sidx)
                            });
                            let onclick_fake_submit = ctx.link().callback(|ee: MouseEvent| {
                                ee.stop_propagation();
                                MultiCalcMsg::OpenMenu(OpenedMenu::None)
                            });

                            let stage = stage.to_owned();

                            html!{
                                <div class={classes!("col-12", "multi-stage", "border-2", "rounded-3", highlight_class)} onclick = {onclick} >
                                    <div class="input-group mb-2">
                                        <span class="input-group-text" style="flex: none; width: 5rem;">{"Name"}</span>
                                        <input
                                            class="form-control"
                                            value = {stage.name.to_owned()}
                                            placeholder = "name, e.g., Stage 3"
                                            oninput = {oninput_name}
                                            onclick = {ctx.link().batch_callback(|ee: MouseEvent| {
                                                ee.stop_propagation();
                                                None
                                            })}
                                        />

                                        <button
                                            type = "button"
                                            class = "btn btn-success float-end"
                                            onclick = {onclick_fake_submit}
                                        >{"Submit"}</button>
                                        <button
                                            type = "button"
                                            class = "btn btn-danger"
                                            style = "flex: none; width: 6rem;"
                                            data-bs-toggle = "modal"
                                            data-bs-target = {format!("#{}", delete_modal_id)}
                                        >{"Delete"}</button>

                                        // Modal for delete dialog
                                        <div
                                            id = {delete_modal_id}
                                            class = "modal fade"
                                            tabindex = "-1"
                                            aria-labeledby = {delete_modal_id_label.clone()}
                                            aria-hidden = "true"
                                        >
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id={delete_modal_id_label}>
                                                            {format!("Do you really wanna delete {}?", stage.name)}
                                                        </h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{"Cancel"}</button>
                                                        <button
                                                            type = "button"
                                                            class = "btn btn-danger"
                                                            onclick = {onclick_stage_delete}
                                                            data-bs-dismiss="modal"
                                                        >{"Yes, delete it!"}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    // Coordinate input elements
                                    <div class="input-group mb-2">
                                        <span class="input-group-text" style="flex: none; width: 3em;">{"Lat"}</span>

                                        <select
                                            class="form-select"
                                            style = "flex: none; max-width: 5em; width: 5em;"
                                            onchange = {onchange_lat_direction}
                                        >
                                            {for Latitude::valid_directions().into_iter().map(|direction| {
                                                html!{
                                                    <option
                                                        value={format!("{}", direction)}
                                                        selected={direction == stage.lat.direction()}
                                                    >
                                                        {direction}
                                                    </option>
                                                }
                                            })}
                                        </select>

                                        <input
                                            type="number"
                                            min="0"
                                            max={format!("{}", Latitude::max_degrees())}
                                            value={format!("{}", stage.lat.degrees())}
                                            class="form-control"
                                            style="flex: none; width: 5em;"
                                            onchange={onchange_lat_degrees}
                                        />
                                        <span class="input-group-text">{"°"}</span>

                                        <input
                                            type="text"
                                            placeholder="12.345 + (A+B) * C"
                                            value= {stage.lat.minutes_input_owned()}
                                            class="form-control"
                                            oninput={oninput_lat_minutes}
                                        />
                                    </div>

                                    <div class="input-group mb-2">
                                        <span class="input-group-text" style="flex: none; width: 3em;">{"Lon"}</span>

                                        <select
                                            class="form-select"
                                            style = "flex: none; max-width: 5em; width: 5em;"
                                            onchange = {onchange_lon_direction}
                                        >
                                            {for Longitude::valid_directions().into_iter().map(|direction| {
                                                html!{
                                                    <option
                                                        value={format!("{}", direction)}
                                                        selected={direction == stage.lon.direction()}
                                                    >
                                                        {direction}
                                                    </option>
                                                }
                                            })}
                                        </select>

                                        <input
                                            type="number"
                                            min="0"
                                            max={format!("{}", Longitude::max_degrees())}
                                            value={format!("{:03}", stage.lon.degrees())}
                                            class="form-control"
                                            style="flex: none; width: 5em;"
                                            onchange={onchange_lon_degrees}
                                        />
                                        <span class="input-group-text">{"°"}</span>

                                        <input
                                            type="text"
                                            placeholder="12.DEF"
                                            value= {stage.lon.minutes_input_owned()}
                                            class="form-control"
                                            oninput={oninput_lon_minutes}
                                        />
                                    </div>


                                    <span class="text-muted">
                                        { match additional_info {
                                            Some(info) => html!{
                                                <>
                                                { format!("Warning: {}\n", info)}
                                                <br/>
                                                </>
                                            },
                                            None => html!{{""}},
                                        } }
                                        { format!("Calculated: {}", stage_coords)}
                                    </span>
                                </div>
                            }
                        } else {
                            let coords_text = match value {
                                Ok(coords) => format!("{}", coords.value()),
                                Err(err) => format!("[Err] {:?}", err),
                            };
                            let onclick_copy_coords = ctx.link().callback_future_once(move |ee: MouseEvent| async move {
                                ee.stop_propagation();

                                let copy_success = copy_to_clipboard(&coords_text).await;
                                MultiCalcMsg::StageCoordsCopied(idx, sidx, copy_success)
                            });

                            let onpointerdown = ctx.link().batch_callback(move |ee: PointerEvent| {
                                if ee.button() == 0 {
                                    ee.prevent_default();
                                    ee.stop_propagation();
                                    Some(MultiCalcMsg::StagePointerDown(idx, sidx))
                                } else {
                                    None
                                }
                            });
                            let onpointerup = ctx.link().batch_callback(move |ee: PointerEvent| {
                                if ee.button() == 0 {
                                    ee.prevent_default();
                                    ee.stop_propagation();
                                    Some(MultiCalcMsg::StagePointerUp(idx, sidx))
                                } else {
                                    // prevents opening the stage menu on right click
                                    None
                                }
                            });

                            html!{
                                <div class = {classes!("col-12", "multi-stage", "border-2", "rounded-3", "clickable", highlight_class)} >
                                    // must be first so that it doesnt cause a line break
                                    <div class="float-end me-2 clickable" onclick = {onclick_copy_coords} >
                                        <IconSvg
                                            height = "1.2em"
                                            icon = {if self.stage_coords_copied == Some((idx, sidx, true)) {
                                                Icon::ClipboardCheck
                                            } else if self.stage_coords_copied == Some((idx, sidx, false)) {
                                                Icon::ClipboardX
                                            } else {
                                                Icon::Clipboard
                                            }}
                                        />
                                    </div>

                                    <div onpointerdown = {onpointerdown} onpointerup = {onpointerup}>
                                        // make this text unselectable, so that long presses (for copying coords to
                                        // clipboard) on mobile devices doesnt select the text.
                                        <span class="unselectable">
                                            {format!(
                                                "{name} ({lat}{number}, {lon}{number}): ",
                                                name = stage.name,
                                                number = sidx,
                                                lat = stage.lat.direction(),
                                                lon = stage.lon.direction(),
                                            )}
                                        </span>
                                        // we dont make the coords unselectable in case there's some issue with the
                                        // copy-to-clipboard logic, etc. This allows the user to still copy the coords.
                                        <span class="fw-bold">{ stage_coords }</span>
                                    </div>
                                </div>
                            }
                        }
                    })}

                    <button
                        type = "button"
                        class = "col-12 multi-stage border-2 rounded-3 text-center btn btn-lg btn-primary"
                        onclick = {ctx.link().callback(move |ee: MouseEvent| {
                            ee.stop_propagation();
                            MultiCalcMsg::AddStage(idx)
                        })}
                    >
                        {"Add stage"}
                    </button>
                    </div>
                </div>

                <hr/>
                <div>
                    <div>
                        <span class = "fs-4" >
                            {"Plaintext notes"}
                            <span class = "fs-6">{" (Use these to save the data to your geocaching notes)"}</span>
                        </span>

                        <div style="display: block;
                        position: relative;
                        float: right;">
                            <button
                                type = "button"
                                class = "btn btn-outline-primary"
                                data-bs-original-title = "Copy to clipboard"
                                style = "position: absolute;
                                top: 3rem;
                                right: .65rem;
                                z-index: 10;
                                display: block;
                                padding: .25rem .5rem;
                                font-size: .65em;
                                width: 6em;"
                                onclick = {ctx.link().callback_future_once(move |ee: MouseEvent| async move {
                                    ee.stop_propagation();

                                    let copy_success = copy_to_clipboard(&plaintext_note_copy).await;
                                    MultiCalcMsg::PlaintextNotesCopied(idx, copy_success)
                                })}
                            >
                                { if self.notes_copied == Some((idx, true)) {
                                    html!{
                                        <>
                                        {"Copied "}
                                        <IconSvg icon = {Icon::ClipboardCheck} height = "1.2em" />
                                        </>
                                    }
                                } else if self.notes_copied == Some((idx, false)) {
                                    html!{
                                        <>
                                        {"Failed "}
                                        <IconSvg icon = {Icon::ClipboardX} height = "1.2em" />
                                        </>
                                    }
                                } else {
                                    html!{
                                        <>
                                        {"Copy "}
                                        <IconSvg icon = {Icon::Clipboard} height = "1.2em" />
                                        </>
                                    }
                                }}
                            </button>
                        </div>
                        <pre
                            class = "form-control"
                            rows = {(plaintext_note.lines().count() + 1).to_string()} // TODO
                            disabled = true
                            readonly = true
                            style = "resize: none;"
                        >
                            {plaintext_note}
                        </pre>
                    </div>
                </div>

                // ensure that there is enough space between the last ui element and the footer, so that the footer
                // doesnt overlay any other UI items
                <div style="height: 2rem;"></div>
            </div>
        }
    }

    fn draw_checksum(
        &self,
        ctx: &Context<Self>,
        multi: &MultiCache,
        multi_idx: usize,
        results: &MultiCacheCalcResult,
    ) -> Html {
        let style = match &results.checksum {
            None => "border: solid var(--bs-gray-600) 1px; background-color: var(--bs-gray-200);",
            Some(ChecksumResult::Fail(..)) => {
                "border: solid var(--bs-red) 1px; background-color: #f1a5aa;"
            }
            Some(ChecksumResult::Pass(..)) => {
                "border: solid var(--bs-green) 1px; background-color: #aaff00;"
            }
            Some(ChecksumResult::FormulaError) => {
                "border: solid var(--bs-orange) 1px; background-color: var(--bs-yellow);"
            }
        };

        if self.data.opened_menu == OpenedMenu::Checksum {
            let stop_propagation = ctx.link().batch_callback(|ee: MouseEvent| {
                ee.stop_propagation();
                None
            });
            let enable_disable = ctx
                .link()
                .callback(move |_: Event| MultiCalcMsg::ChecksumToggle(multi_idx));

            let ((sum_enabled, val_sum), (formula_enabled, val_formula_left, val_formula_right)) =
                match &multi.checksum {
                    Some(Checksum::Sum(value)) => {
                        let formula = match &self.old_checksum {
                            Checksum::Formula(left, right) => (left.to_owned(), right.to_owned()),
                            _ => Default::default(),
                        };
                        ((true, value.to_string()), (false, formula.0, formula.1))
                    }
                    Some(Checksum::Formula(left, right)) => {
                        let sum = match &self.old_checksum {
                            Checksum::Sum(sum) => *sum,
                            _ => 0,
                        };
                        (
                            (false, sum.to_string()),
                            (true, left.to_owned(), right.to_owned()),
                        )
                    }
                    _ => {
                        let (sum, formula_left, formula_right) = match &self.old_checksum {
                            Checksum::Formula(left, right) => {
                                (0, left.to_owned(), right.to_owned())
                            }
                            Checksum::Sum(sum) => (*sum, String::new(), String::new()),
                        };
                        (
                            (false, sum.to_string()),
                            (false, formula_left, formula_right),
                        )
                    }
                };

            let onclick_select_sum = ctx.link().batch_callback(move |_: MouseEvent| {
                if sum_enabled {
                    None
                } else {
                    Some(MultiCalcMsg::ChecksumSetMethodSum(multi_idx))
                }
            });
            let onclick_select_formula = ctx.link().batch_callback(move |_: MouseEvent| {
                if formula_enabled {
                    None
                } else {
                    Some(MultiCalcMsg::ChecksumSetMethodFormula(multi_idx))
                }
            });

            let onchange_value_sum = ctx.link().batch_callback(move |ee: Event| {
                ee.target_dyn_into::<HtmlInputElement>()
                    .map(|input| MultiCalcMsg::ChecksumUpdateSumValue(multi_idx, input.value()))
            });
            let oninput_value_formula_left = ctx.link().batch_callback(move |ee: InputEvent| {
                ee.target_dyn_into::<HtmlInputElement>()
                    .map(|input| MultiCalcMsg::ChecksumUpdateFormulaValueLeft(multi_idx, input.value()))
            });
            let oninput_value_formula_right = ctx.link().batch_callback(move |ee: InputEvent| {
                ee.target_dyn_into::<HtmlInputElement>()
                    .map(|input| MultiCalcMsg::ChecksumUpdateFormulaValueRight(multi_idx, input.value()))
            });
            // Data is auto-saved as soon as user enters something. The user could also exit the
            // form by just clicking anywhere but it's probably more intuitive (and sometimes
            // easier) to have a dedicated button for it. When clicking it, we'll just close the
            // current menu.
            let onclick_fake_submit = ctx.link().callback(|ee: MouseEvent| {
                ee.stop_propagation();
                MultiCalcMsg::OpenMenu(OpenedMenu::None)
            });

            html! {
                <div class="border-2 rounded-3" style = {style} >
                <div onclick = {stop_propagation} class = "fs-5">
                    <div class = "form-check form-switch">
                        <input
                            class = "form-check-input"
                            type = "checkbox"
                            id = "checksumEnabled"
                            checked = {multi.checksum.is_some()}
                            onchange = {enable_disable}
                        />
                        <label class = "form-check-label" for = "checksumEnabled">
                            {"Enable checksum"}
                        </label>

                        <button
                            type = "button"
                            class = "btn btn-success float-end"
                            onclick = {onclick_fake_submit}
                        >{"Submit"}</button>
                    </div>

                    <hr/>

                    <div class = "form-check">
                        <input
                            class = "form-check-input"
                            type = "radio"
                            name = "checksumMethod"
                            id = "checksumMethodSum"
                            disabled = {multi.checksum.is_none()}
                            checked = {sum_enabled}
                            onclick = {onclick_select_sum}
                        />
                        <label class = "form-check-label" for = "checksumMethodSum">
                            <div class = "input-group">
                                <span class = "input-group-text">{"Sum"}</span>
                                <input
                                    type = "number"
                                    class = "form-control"
                                    value = {val_sum}
                                    disabled = {!sum_enabled}
                                    onchange = {onchange_value_sum.clone()}
                                />
                            </div>
                        </label>
                    </div>

                    <div class = "form-check">
                        <input
                            class = "form-check-input"
                            type = "radio"
                            name = "checksumMethod"
                            id = "checksumMethodFormula"
                            disabled = {multi.checksum.is_none()}
                            checked = {formula_enabled}
                            onclick = {onclick_select_formula}
                        />
                        <label class = "form-check-label" for = "checksumMethodFormula">
                            <div class = "input-group">
                            <span class = "input-group-text">{"Formula"}</span>
                            <input
                                type = "text"
                                class = "form-control"
                                value = {val_formula_left}
                                disabled = {!formula_enabled}
                                oninput = {oninput_value_formula_left}
                            />
                            <span class = "input-group-text">{"="}</span>
                            <input
                                type = "text"
                                class = "form-control"
                                value = {val_formula_right}
                                disabled = {!formula_enabled}
                                oninput = {oninput_value_formula_right}
                            />
                        </div>
                        </label>
                    </div>
                </div>
                </div>
            }
        } else {
            let onclick = ctx.link().callback(|ee: MouseEvent| {
                ee.stop_propagation();
                MultiCalcMsg::OpenMenu(OpenedMenu::Checksum)
            });

            let details = match &results.checksum {
                Some(ChecksumResult::Pass(left, right)) => {
                    format!(": {} = {}", left, right)
                }
                Some(ChecksumResult::Fail(left, right)) => {
                    format!(": {} != {}", left, right)
                }
                Some(ChecksumResult::FormulaError) => ": [Err!]".to_string(),
                None => "".to_string(),
            };

            html! {
                <button
                    type = "button"
                    class="multi-variable border-2 rounded-3 btn btn-lg"
                    style = {style}
                    onclick = {onclick}
                >{format!("Checksum{}", details)}</button>
            }
        }
    }
}

impl Component for MultiCalcPage {
    type Message = MultiCalcMsg;
    type Properties = MultiCalcProps;

    fn create(ctx: &Context<Self>) -> Self {
        // Yew doesnt provide a "native" way to assign a handler to the Window.onhashchange event handler
        // Therefore, we need to create it ourselves using web_sys & wasm_bindgen
        // TODO: Maybe we should read out the hash within the closure and provide it via the message. This would allow
        // us to test the handler for the HashUpdated message
        if let Some(window) = web_sys::window() {
            let callback = ctx.link().callback(|_: ()| Self::Message::HashUpdated);
            let closure = wasm_bindgen::closure::Closure::wrap(Box::new(move || {
                callback.emit(());
            }) as Box<dyn FnMut()>)
            .into_js_value();

            let function = closure.dyn_ref();
            window.set_onhashchange(function);
        };

        Self {
            data: ctx.props().data.clone(),
            stage_coords_copied: None,
            stage_onpointerdown_data: None,
            variable_errors: Vec::new(),
            old_checksum: Checksum::Sum(0),
            notes_copied: None,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        let storage = LocalStorage::new();

        let result = match msg {
            Self::Message::AddStage(cidx) => {
                self.data.caches[cidx].add_stage();
                self.data.opened_menu =
                    OpenedMenu::Stage(self.data.caches[cidx].stages.len() - 1);
                true
            }
            Self::Message::AddVariable(cidx) => {
                self.data.caches[cidx].add_variable();
                self.data.opened_menu =
                    OpenedMenu::Variable(self.data.caches[cidx].variables.len() - 1);
                true
            }
            Self::Message::ChecksumSetMethodSum(cidx) => {
                let new_value = if let Checksum::Sum(_) = self.old_checksum {
                    self.old_checksum.clone()
                } else {
                    Checksum::Sum(0)
                };

                self.old_checksum = self.data.caches[cidx]
                    .checksum
                    .clone()
                    .unwrap_or(Checksum::Sum(0));
                self.data.caches[cidx]
                    .checksum
                    .neq_assign(Some(new_value))
            }
            Self::Message::ChecksumSetMethodFormula(cidx) => {
                let new_value = if let Checksum::Formula(..) = self.old_checksum {
                    self.old_checksum.clone()
                } else {
                    Checksum::Formula(String::new(), String::new())
                };

                self.old_checksum = self.data.caches[cidx]
                    .checksum
                    .clone()
                    .unwrap_or(Checksum::Sum(0));
                self.data.caches[cidx]
                    .checksum
                    .neq_assign(Some(new_value))
            }
            Self::Message::ChecksumUpdateSumValue(cidx, new_value) => {
                let checksum = &mut self.data.caches[cidx].checksum;

                if let Some(Checksum::Sum(ref mut old_value)) = checksum {
                    if let Ok(new_value) = new_value.parse() {
                        old_value.neq_assign(new_value)
                    } else {
                        false
                    }
                } else {
                    false
                }
            }
            Self::Message::ChecksumUpdateFormulaValueLeft(cidx, mut new_value) => {
                let checksum = &mut self.data.caches[cidx].checksum;

                if let Some(Checksum::Formula(ref mut old_value, _)) = checksum {
                    new_value.make_ascii_uppercase();
                    old_value.neq_assign(new_value)
                } else {
                    false
                }
            }
            Self::Message::ChecksumUpdateFormulaValueRight(cidx, mut new_value) => {
                let checksum = &mut self.data.caches[cidx].checksum;

                if let Some(Checksum::Formula(_, ref mut old_value)) = checksum {
                    new_value.make_ascii_uppercase();
                    old_value.neq_assign(new_value)
                } else {
                    false
                }
            }
            Self::Message::ChecksumToggle(cidx) => {
                if self.data.caches[cidx].checksum.is_some() {
                    self.old_checksum = self.data.caches[0].checksum.clone().unwrap();
                    self.data.caches[cidx].checksum = None;
                } else {
                    self.data.caches[cidx].checksum = Some(self.old_checksum.clone());
                }

                true
            }
            Self::Message::DeleteStage(cidx, sidx) => {
                let stages = &mut self.data.caches[cidx].stages;
                if stages.len() > sidx {
                    stages.remove(sidx);
                    self.data.opened_menu = OpenedMenu::None;
                }

                true
            }
            Self::Message::DeleteVariable(cidx, vidx) => {
                let variables = &mut self.data.caches[cidx].variables;
                if variables.len() > vidx {
                    variables.remove(vidx);
                    self.data.opened_menu = OpenedMenu::None;
                }

                true
            }
            Self::Message::HashUpdated => {
                let result = match web_sys::window() {
                    Some(window) => match window.location().hash() {
                        Ok(hash) => match Urldata::multicache_from_urlencoded_json(&hash) {
                            Some(multicache) => self.data.caches[0].neq_assign(multicache),
                            None => false,
                        },
                        Err(_) => false,
                    },
                    None => false,
                };

                if !result {
                    let _ = web_sys::window().map(|window| {
                        window
                            .location()
                            .set_hash(&Urldata::from(&self.data.caches[0]).into_json())
                    });
                }

                result
            }
            Self::Message::OpenMenu(opened_menu) => {
                let result = self.data.opened_menu.neq_assign(opened_menu);

                if result {
                    self.variable_errors.clear();
                }

                result
            }
            Self::Message::PlaintextNotesCopied(cidx, success) => {
                let result = self.notes_copied.neq_assign(Some((cidx, success)));

                if result {
                    let link = ctx.link().to_owned();
                    let timeout = Timeout::new(3_000, move || {
                        link.callback(|_: ()| Self::Message::PlaintextNotesCopiedReset);
                    });

                    timeout.forget();
                }

                result
            }
            Self::Message::PlaintextNotesCopiedReset => {
                self.notes_copied = None;
                true
            }
            Self::Message::Reset => self.data.caches[0].neq_assign(MultiCache::default()),
            Self::Message::StageCoordsCopied(cidx, sidx, success) => {
                let result = self
                    .stage_coords_copied
                    .neq_assign(Some((cidx, sidx, success)));

                if result {
                    let link = ctx.link().to_owned();
                    let timeout = Timeout::new(3_000, move || {
                        link.callback(|_: ()| Self::Message::StageCoordsCopiedReset);
                    });

                    timeout.forget();
                }

                result
            }
            Self::Message::StageCoordsCopiedReset => {
                self.stage_coords_copied = None;
                true
            }
            Self::Message::StagePointerDown(cidx, sidx) => {
                let update =
                    self.stage_onpointerdown_data
                        .neq_assign(Some((cidx, sidx, Utc::now())));

                if update {
                    /*self.stage_coords_timeout_handle = Some(yew::services::TimeoutService::spawn(
                        std::time::Duration::from_secs(1),
                        self.link.callback_future_once(move |_| async move {
                            let copy_success = copy_to_clipboard("asdf").await;
                            MultiCalcMsg::StageCoordsCopied(cidx, sidx, copy_success)
                        }),
                    ));*/

                    let link = ctx.link().to_owned();
                    spawn_local(async move {
                        TimeoutFuture::new(1_000).await;
                        let copy_success = copy_to_clipboard("asdf").await;
                        link.callback(move |_: ()| MultiCalcMsg::StageCoordsCopied(cidx, sidx, copy_success));
                    });
                }

                update
            }
            Self::Message::StagePointerUp(cidx, sidx) => {
                let clear_pointerdata = match self.stage_onpointerdown_data {
                    Some((data_cidx, data_sidx, start_time)) => {
                        let now = Utc::now();
                        cidx == data_cidx
                            && sidx == data_sidx
                            && now - start_time > Duration::seconds(1)
                    }
                    None => false,
                };

                self.stage_onpointerdown_data = None;

                if clear_pointerdata {
                    true
                } else {
                    self.update(ctx, MultiCalcMsg::OpenMenu(OpenedMenu::Stage(sidx)))
                }
            }
            Self::Message::UpdateCacheCode(cidx, gccode) => {
                let _ = self.data.caches[cidx].id.set_to(gccode);
                true
            }
            Self::Message::UpdateCacheName(cidx, name) => {
                self.data.caches[cidx].name.neq_assign(name)
            }
            Self::Message::UpdateStageLatDirection(cidx, sidx, direction) => {
                self.data.caches[cidx].stages[sidx]
                    .lat
                    .set_direction(direction);
                storage.set(StorageItem::LatitudeDirection, direction);
                true
            }
            Self::Message::UpdateStageLatDegrees(cidx, sidx, value) => match value.parse() {
                Ok(degrees) => {
                    self.data.caches[cidx].stages[sidx]
                        .lat
                        .set_degrees(degrees);
                    storage.set(StorageItem::LatitudeDegrees, degrees);
                    true
                }
                Err(_) => false,
            },
            Self::Message::UpdateStageLatMinutes(cidx, sidx, minutes_input) => {
                self.data.caches[cidx].stages[sidx]
                    .lat
                    .set_minutes_input(minutes_input);
                true
            }
            Self::Message::UpdateStageLonDirection(cidx, sidx, direction) => {
                self.data.caches[cidx].stages[sidx]
                    .lon
                    .set_direction(direction);
                storage.set(StorageItem::LongitudeDirection, direction);
                true
            }
            Self::Message::UpdateStageLonDegrees(cidx, sidx, value) => match value.parse() {
                Ok(degrees) => {
                    self.data.caches[cidx].stages[sidx]
                        .lon
                        .set_degrees(degrees);
                    storage.set(StorageItem::LongitudeDegrees, degrees);
                    true
                }
                Err(_) => false,
            },
            Self::Message::UpdateStageLonMinutes(cidx, sidx, minutes_input) => {
                self.data.caches[cidx].stages[sidx]
                    .lon
                    .set_minutes_input(minutes_input);
                true
            }
            Self::Message::UpdateStageName(cidx, sidx, name) => self.data.caches[cidx].stages
                [sidx]
                .name
                .neq_assign(name),
            Self::Message::UpdateVariableAnswer(cidx, vidx, new_answer) => {
                let variable = &mut self.data.caches[cidx].variables[vidx];
                let result = variable.set_answer(new_answer);

                match (
                    result,
                    variable.answer().starts_with('='),
                    variable.calc_method(),
                ) {
                    (true, true, _) => {
                        let _ = variable.set_calc_method(VariableCalculationMethod::Formula);
                    }
                    (true, false, VariableCalculationMethod::Formula) => {
                        let _ = variable.set_calc_method(VariableCalculationMethod::Raw);
                    }
                    _ => {}
                }

                result
            }
            Self::Message::UpdateVariableCalculationMethod(cidx, vidx, calculation_method) => {
                self.data.caches[cidx].variables[vidx].set_calc_method(calculation_method)
            }
            Self::Message::UpdateVariableName(cidx, vidx, name) => {
                let (update, errors) = self.data.caches[cidx].variables[vidx].set_name(name);
                self.variable_errors.neq_assign(errors) || update
            }
        };

        if result {
            let _ = web_sys::window().map(|window| {
                window
                    .location()
                    .set_hash(&Urldata::from(&self.data.caches[0]).into_json())
            });
        }

        result
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
            <div onclick = {ctx.link().callback(|_| MultiCalcMsg::OpenMenu(OpenedMenu::None))} style="height: 100%;">
                {for self.data.caches.iter().enumerate().map(|(idx, _cache)|{
                    self.draw_cache(ctx, idx)
                })}
            </div>
        }
    }
}

impl MultiCache {
    fn add_stage(&mut self) {
        self.stages.push(Stage::default());

        let name = if self.stages.len() <= 1 {
            "Header".to_string()
        } else {
            format!("Stage {}", self.stages.len() - 1)
        };

        if let Some(stage) = self.stages.last_mut() {
            stage.name = name;

            let storage = LocalStorage::new();
            stage.lat.set_direction(storage.get(StorageItem::LatitudeDirection).unwrap_or(Direction::North));
            stage.lat.set_degrees(storage.get(StorageItem::LatitudeDegrees).unwrap_or_default());
            stage.lon.set_direction(storage.get(StorageItem::LongitudeDirection).unwrap_or(Direction::East));
            stage.lon.set_degrees(storage.get(StorageItem::LongitudeDegrees).unwrap_or_default());
        }
    }

    fn add_variable(&mut self) {
        let mut var = Variable::default();

        // compile list of existing variables, so that we do not create a duplicate
        #[allow(clippy::needless_collect)]
        // false positive, see https://github.com/rust-lang/rust-clippy/issues/6164
        let variable_names: Vec<_> = self.variables.iter().map(|var| var.name()).collect();

        // for the first 26 variables, we will simply go from A to Z taking the first available one
        // Afterwards AA .. AZ, then BA .. BZ, CA .. CZ, and so on
        // Yes, this may lead to a large number of string allocations but it should still be fast enough in most cases
        let mut prefix = "".to_string();
        let name = 'name_loop: loop {
            for cc in 'A'..='Z' {
                let name_to_check = format!("{}{}", prefix, cc);
                if !variable_names.contains(&name_to_check.as_str()) {
                    break 'name_loop name_to_check;
                }
            }

            match prefix.chars().last() {
                None => prefix = "A".to_string(),
                Some(last @ 'A'..='Y') => {
                    prefix = prefix.strip_suffix(last).unwrap().to_string();
                    // unwrap is safe because we only have valid ascii characters
                    prefix.push(char::from_u32(last as u32 + 1).unwrap());
                }
                Some('Z') => {
                    prefix = prefix.strip_suffix('Z').unwrap().to_string() + "AA";
                }
                Some(last) => unreachable!(
                    "Prefix contained unexpected letter at the end: {} ({})",
                    last, prefix
                ),
            }
        };
        var.set_name(name);

        self.variables.push(var);
    }

    fn calc_all(&self) -> MultiCacheCalcResult<'_> {
        let mut variables_return = Vec::new();
        let mut variables_for_calculation =
            VecDeque::with_capacity(1 + self.variables.len() + self.stages.len() * 2);

        // TODO: Currently, each variable can only reference variables which are created before itself
        for var in &self.variables {
            let result = var.value(&variables_for_calculation);
            if var.has_name() {
                let item_to_add = (
                    var.name().to_string(),
                    match &result {
                        Ok(value) => value.clone(),
                        Err(error) => Value::default_with_info(error.clone()),
                    },
                    VariableKind::Variable,
                );

                // We order all variables by the length of their name (descending). This prevents us from replacing
                // parts of variables. E.g., if there are the variables "A", "B", "AB", and "ATMP" we would prevent that
                // the string "AB" is translated to "<value of A><value of B>" (because the translation step just
                // goes over the list blindly)
                let varlen = var.name().len();
                match variables_for_calculation
                    .iter()
                    .position(|(var, _, _): &(String, _, _)| var.len() <= varlen)
                {
                    Some(idx) => variables_for_calculation.insert(idx, item_to_add),
                    None => variables_for_calculation.push_back(item_to_add),
                }
            }
            variables_return.push((var, result));
        }

        // Also add the special SUM variable to the list
        let sum = (
            "SUM".to_owned(),
            Value::new(
                variables_for_calculation
                    .iter()
                    .filter_map(|(name, value, _)| {
                        if !Variable::name_matches_reserved_pattern(name) {
                            Some(value.value())
                        } else {
                            None
                        }
                    })
                    .sum(),
            ),
            VariableKind::Sum,
        );
        match variables_for_calculation
            .iter()
            .position(|(var, _, _): &(String, _, _)| var.len() <= sum.0.len())
        {
            Some(idx) => variables_for_calculation.insert(idx, sum),
            None => variables_for_calculation.push_back(sum),
        }

        let mut stages_return = Vec::new();
        for (sidx, stage) in self.stages.iter().enumerate() {
            let result = stage.coords(&variables_for_calculation);

            if let Ok(coords) = result.as_ref() {
                let coords_value = coords.value();
                let lat_name = format!("{}{}", coords_value.lat().direction(), sidx);
                let lon_name = format!("{}{}", coords_value.lon().direction(), sidx);

                if coords.info().is_some() {
                    variables_for_calculation.push_front((
                        lat_name,
                        Value::default(),
                        VariableKind::Stage,
                    ));
                    variables_for_calculation.push_front((
                        lon_name,
                        Value::default(),
                        VariableKind::Stage,
                    ));
                } else {
                    let lat_value =
                        coords_value.lat().minutes() * 1000 + coords_value.lat().subminutes();
                    let lon_value =
                        coords_value.lon().minutes() * 1000 + coords_value.lon().subminutes();

                    // For convenience we allow the user to name variables like stage variables. But we'll ensure that the
                    // actual stage variables are always in front of the normal variables (user variables will be ignored)
                    variables_for_calculation.push_front((
                        lat_name,
                        Value::new(lat_value),
                        VariableKind::Stage,
                    ));
                    variables_for_calculation.push_front((
                        lon_name,
                        Value::new(lon_value),
                        VariableKind::Stage,
                    ));
                }
            }

            stages_return.push((stage, result));
        }

        MultiCacheCalcResult {
            variables: variables_return,
            stages: stages_return,
            checksum: self
                .checksum
                .as_ref()
                .map(|checksum| checksum.check(&variables_for_calculation)),
            all_variables_list: variables_for_calculation,
        }
    }

    fn plaintext_note(&self, calc_results: &MultiCacheCalcResult) -> String {
        let mut result = format!("{} - {}\n", self.id, self.name);

        let variables_lookup: HashMap<_, _> = calc_results
            .variables
            .iter()
            .map(|(var, result)| (var.name(), result))
            .collect();
        let stages_lookup: HashMap<_, _> = calc_results
            .stages
            .iter()
            .map(|(stage, result)| (&stage.name, result))
            .collect();

        result.push_str("\nVariables:\n");
        for var in &self.variables {
            if !var.has_name() {
                continue;
            }

            let value = match variables_lookup.get(var.name()) {
                Some(Ok(value)) => {
                    // If user hasn't entered anything yet, the "calculated value" will be 0. But here we want to print
                    // nothing to show that the user actually hasnt entered anything
                    if var.answer().trim().is_empty() {
                        "".to_string()
                    } else {
                        value.value().to_string()
                    }
                }
                Some(Err(_)) | None => "??".to_string(),
            };

            // If an actual "calculation mode" (except Raw) is used and the user entered something OR if we were unable
            // to calculate the value, show the user input
            let value_info_str = if var.calc_method() != VariableCalculationMethod::Raw
                && !value.is_empty()
                || value == "??"
            {
                format!(" ({})", var.answer())
            } else {
                "".to_string()
            };

            let varstring = format!("{} = {}{}\n", var.name(), value, value_info_str);
            result.push_str(&varstring);
        }

        if let Some(checksum_result) = &calc_results.checksum {
            let value_info = match &self.checksum {
                Some(Checksum::Sum(value)) => value.to_string(),
                Some(Checksum::Formula(left, right)) => format!("{} = {}", left, right),
                None => "".to_string(),
            };

            let result_info = match checksum_result {
                ChecksumResult::Pass(left, right) => format!("Passed ({} = {})", left, right),
                ChecksumResult::Fail(left, right) => format!("Failed ({} != {})", left, right),
                ChecksumResult::FormulaError => "Formula error".to_string(),
            };

            let checksum_string = format!("\nChecksum ({}): {}\n", value_info, result_info);
            result.push_str(&checksum_string);
        }

        result.push_str("\nStages:\n");
        for stage in &self.stages {
            let stage_coords = match stages_lookup.get(&stage.name) {
                Some(Ok(coords)) => format!("{}", coords.value()),
                Some(Err(_)) | None => "??".to_string(),
            };

            let stage_string = format!("{} = {}\n", stage.name, stage_coords);
            result.push_str(&stage_string);
        }

        let version_info = format!(
            "\n\nGC Web Tools MultiCalc Info\nVersion = {}\n",
            crate::util::version_string(),
        );
        let url_info = match web_sys::window() {
            Some(window) => match window.location().origin() {
                Ok(origin) => {
                    let jsondata = Urldata::from(self).into_json();
                    let fragment = urlencoding::encode(&jsondata);
                    format!("URL = {}/#{}\n", origin, fragment)
                }
                Err(_) => "".to_string(),
            },
            None => "".to_string(),
        };

        result.push_str(&version_info);
        result.push_str(&url_info);

        result
    }
}

impl Default for MultiCache {
    fn default() -> Self {
        Self {
            id: GeoCode::new("D3FLT".to_string()),
            name: "Default cache name".to_string(),
            variables: Vec::new(),
            stages: Vec::new(),
            checksum: None,
        }
    }
}

impl MultiCacheCalcResult<'_> {
    pub fn get_sum(&self) -> Value<i32> {
        self.all_variables_list
            .iter()
            .find(|(name, _, kind)| name == "SUM" && *kind == VariableKind::Sum)
            .map(|(_, value, _)| value.to_owned())
            .unwrap_or_default()
    }
}

impl Default for OpenedMenu {
    fn default() -> Self {
        Self::None
    }
}

impl Stage {
    fn coords<S: AsRef<str>>(
        &self,
        variables: &VecDeque<(S, Value<i32>, VariableKind)>,
    ) -> Result<Value<CalculatedCoordinate>, CoordinateError> {
        let lat = self.lat.value(variables)?;
        let lon = self.lon.value(variables)?;

        let value = Value::new(CalculatedCoordinate::new(lat.value(), lon.value()))
            .with_info(lat.info().clone().or_else(|| lon.info().clone()));

        Ok(value)
    }
}

impl Sanitize for Stage {
    fn sanitized(mut self) -> (Self, Vec<(&'static str, FormatError)>) {
        let mut errors = Vec::new();

        let (new_lat, mut lat_errors) = self.lat.sanitized();
        self.lat = new_lat;
        errors.append(&mut lat_errors); // TODO: We should prefix all with "lat.", but this cannot be done with &str's

        let (new_lon, mut lon_errors) = self.lon.sanitized();
        self.lon = new_lon;
        errors.append(&mut lon_errors); // TODO: We should prefix all with "lon.", but this cannot be done with &str's

        (self, errors)
    }
}

#[cfg(test)]
mod tests {
    use super::coord::CalculatedCoordinatePart;
    use super::*;
    use pretty_assertions::assert_eq;

    /// Covers a known bug which lead to incorrect calculation of coordinates
    #[test]
    fn coord_calculation_unwanted_rounding() {
        let variables = vec![
            Variable::from(("ATMP", "skidata", VariableCalculationMethod::SumOfDigits)),
            Variable::from(("A", "= ATMP + 95", VariableCalculationMethod::Formula)),
            Variable::from(("B", "12", VariableCalculationMethod::Raw)),
            Variable::from(("C", "61", VariableCalculationMethod::Raw)),
            Variable::from(("N1", "= (B+3) * 1000", VariableCalculationMethod::Formula)),
            Variable::from(("N2", "= A*2-48", VariableCalculationMethod::Formula)),
            Variable::from(("E1", "= (B*2+4) * 1000", VariableCalculationMethod::Formula)),
            Variable::from(("E2", "= C*10+17", VariableCalculationMethod::Formula)),
        ];
        let stage = Stage {
            name: "Final".to_string(),
            lat: Latitude::with(Direction::North, 42, "00.000 + N1+N2"),
            lon: Longitude::with(Direction::East, 42, "00.000 + E1 + E2"),
        };
        let expected = Ok(Value::new(CalculatedCoordinate::new(
            CalculatedCoordinatePart::with(Direction::North, 42, 15, 272),
            CalculatedCoordinatePart::with(Direction::East, 42, 28, 627),
        )));

        let result = helper_calculate_single_stage(variables, stage);
        assert_eq!(expected, result, "Coordinate calculation failed");
    }

    #[test]
    fn coord_calc_basic_addition() {
        // see: https://gitlab.com/BafDyce/gc-web-tools/-/issues/15
        // http://127.0.0.1:8080/#{%22id%22:%22D3FLT%22,%22name%22:%22Default%20cache%20name%22,%22variables%22:[{%22name%22:%22A%22,%22answer%22:%2242%22,%22calc_method%22:%22Raw%22}],%22stages%22:[{%22name%22:%22Header%22,%22lat%22:{%22direction%22:%22North%22,%22degrees%22:48,%22minutes_input%22:%22A%22},%22lon%22:{%22direction%22:%22East%22,%22degrees%22:16,%22minutes_input%22:%2212.000%20+%20A%22}}],%22version%22:%22V0_1%22}
        let variables = vec![Variable::from(("A", "42", VariableCalculationMethod::Raw))];
        let stage = Stage {
            name: "Final".to_string(),
            lat: Latitude::with(Direction::North, 42, "A"),
            lon: Longitude::with(Direction::East, 42, "12.000 + A"),
        };

        let expected = Ok(Value::new(CalculatedCoordinate::new(
            CalculatedCoordinatePart::with(Direction::North, 42, 0, 42),
            CalculatedCoordinatePart::with(Direction::East, 42, 12, 42),
        )));

        let result = helper_calculate_single_stage(variables, stage);
        assert_eq!(expected, result, "Coordinate calculation failed");
    }

    #[test]
    fn coord_calc_variable_as_subminutes() {
        // see: https://gitlab.com/BafDyce/gc-web-tools/-/issues/15 (2nd image)
        // http://127.0.0.1:8080/#{%22id%22:%22D3FLT%22,%22name%22:%22Default%20cache%20name%22,%22variables%22:[{%22name%22:%22A%22,%22answer%22:%223%22,%22calc_method%22:%22Raw%22},{%22name%22:%22B%22,%22answer%22:%225%22,%22calc_method%22:%22Raw%22},{%22name%22:%22C%22,%22answer%22:%227%22,%22calc_method%22:%22Raw%22},{%22name%22:%22D%22,%22answer%22:%2223%22,%22calc_method%22:%22Raw%22}],%22stages%22:[{%22name%22:%22Header%22,%22lat%22:{%22direction%22:%22North%22,%22degrees%22:42,%22minutes_input%22:%2223.ABC%22},%22lon%22:{%22direction%22:%22East%22,%22degrees%22:42,%22minutes_input%22:%2210.1D%22}}],%22version%22:%22V0_1%22}
        let variables = vec![
            Variable::from(("A", "3", VariableCalculationMethod::Raw)),
            Variable::from(("B", "5", VariableCalculationMethod::Raw)),
            Variable::from(("C", "7", VariableCalculationMethod::Raw)),
            Variable::from(("D", "23", VariableCalculationMethod::Raw)),
        ];
        let stage = Stage {
            name: "Final".to_string(),
            lat: Latitude::with(Direction::North, 42, "23.ABC"),
            lon: Longitude::with(Direction::East, 42, "10.1D"),
        };

        let expected = Ok(Value::new(CalculatedCoordinate::new(
            CalculatedCoordinatePart::with(Direction::North, 42, 23, 357),
            CalculatedCoordinatePart::with(Direction::East, 42, 10, 123),
        )));

        let result = helper_calculate_single_stage(variables, stage);
        assert_eq!(expected, result, "Coordinate calculation failed");
    }

    #[test]
    fn coord_calc_variable_as_subminutes_2() {
        // http://127.0.0.1:8080/#{%22id%22:%22D3FLT%22,%22name%22:%22Default%20cache%20name%22,%22variables%22:[{%22name%22:%22A%22,%22answer%22:%223%22,%22calc_method%22:%22Raw%22},{%22name%22:%22B%22,%22answer%22:%225%22,%22calc_method%22:%22Raw%22},{%22name%22:%22C%22,%22answer%22:%227%22,%22calc_method%22:%22Raw%22},{%22name%22:%22D%22,%22answer%22:%2223%22,%22calc_method%22:%22Raw%22}],%22stages%22:[{%22name%22:%22Header%22,%22lat%22:{%22direction%22:%22North%22,%22degrees%22:42,%22minutes_input%22:%22D.ABC%22},%22lon%22:{%22direction%22:%22East%22,%22degrees%22:42,%22minutes_input%22:%221A.1D%22}}],%22version%22:%22V0_1%22}
        let variables = vec![
            Variable::from(("A", "3", VariableCalculationMethod::Raw)),
            Variable::from(("B", "5", VariableCalculationMethod::Raw)),
            Variable::from(("C", "7", VariableCalculationMethod::Raw)),
            Variable::from(("D", "23", VariableCalculationMethod::Raw)),
        ];
        let stage = Stage {
            name: "Final".to_string(),
            lat: Latitude::with(Direction::North, 42, "D.ABC"),
            lon: Longitude::with(Direction::East, 42, "1A.1D"),
        };

        let expected = Ok(Value::new(CalculatedCoordinate::new(
            CalculatedCoordinatePart::with(Direction::North, 42, 23, 357),
            CalculatedCoordinatePart::with(Direction::East, 42, 13, 123),
        )));
        let result = helper_calculate_single_stage(variables, stage);
        assert_eq!(expected, result, "Coordinate calculation failed");
    }

    #[test]
    fn coord_calc_stage_only_vars() {
        // http://127.0.0.1:8080/#{%22id%22:%22D3FLT%22,%22name%22:%22Default%20cache%20name%22,%22variables%22:[{%22name%22:%22A%22,%22answer%22:%220%22,%22calc_method%22:%22Raw%22},{%22name%22:%22B%22,%22answer%22:%221%22,%22calc_method%22:%22Raw%22},{%22name%22:%22C%22,%22answer%22:%222%22,%22calc_method%22:%22Raw%22},{%22name%22:%22D%22,%22answer%22:%223%22,%22calc_method%22:%22Raw%22},{%22name%22:%22E%22,%22answer%22:%224%22,%22calc_method%22:%22Raw%22},{%22name%22:%22F%22,%22answer%22:%225%22,%22calc_method%22:%22Raw%22},{%22name%22:%22G%22,%22answer%22:%226%22,%22calc_method%22:%22Raw%22},{%22name%22:%22H%22,%22answer%22:%227%22,%22calc_method%22:%22Raw%22},{%22name%22:%22I%22,%22answer%22:%228%22,%22calc_method%22:%22Raw%22},{%22name%22:%22J%22,%22answer%22:%229%22,%22calc_method%22:%22Raw%22}],%22stages%22:[{%22name%22:%22Header%22,%22lat%22:{%22direction%22:%22North%22,%22degrees%22:42,%22minutes_input%22:%22AB.CDE%22},%22lon%22:{%22direction%22:%22East%22,%22degrees%22:42,%22minutes_input%22:%22FG.HIJ%22}}],%22version%22:%22V0_1%22}
        let variables = vec![
            Variable::from(("A", "0", VariableCalculationMethod::Raw)),
            Variable::from(("B", "1", VariableCalculationMethod::Raw)),
            Variable::from(("C", "2", VariableCalculationMethod::Raw)),
            Variable::from(("D", "3", VariableCalculationMethod::Raw)),
            Variable::from(("E", "4", VariableCalculationMethod::Raw)),
            Variable::from(("F", "5", VariableCalculationMethod::Raw)),
            Variable::from(("G", "6", VariableCalculationMethod::Raw)),
            Variable::from(("H", "7", VariableCalculationMethod::Raw)),
            Variable::from(("I", "8", VariableCalculationMethod::Raw)),
            Variable::from(("J", "9", VariableCalculationMethod::Raw)),
        ];
        let stage = Stage {
            name: "Final".to_string(),
            lat: Latitude::with(Direction::North, 42, "AB.CDE"),
            lon: Longitude::with(Direction::East, 42, "FG.HIJ"),
        };

        let expected = Ok(Value::new(CalculatedCoordinate::new(
            CalculatedCoordinatePart::with(Direction::North, 42, 1, 234),
            CalculatedCoordinatePart::with(Direction::East, 42, 56, 789),
        )));
        let result = helper_calculate_single_stage(variables, stage);
        assert_eq!(expected, result, "Coordinate calculation failed");
    }

    #[test]
    fn coord_calc_overlapping_variables() {
        // https://127.0.0.1:8080/#{%22id%22:%22D3FLT%22,%22name%22:%22Default%20cache%20name%22,%22variables%22:[{%22name%22:%22A%22,%22answer%22:%221%22,%22calc_method%22:%22Raw%22},{%22name%22:%22B%22,%22answer%22:%222%22,%22calc_method%22:%22Raw%22},{%22name%22:%22AB%22,%22answer%22:%2233%22,%22calc_method%22:%22Raw%22},{%22name%22:%22ATMP%22,%22answer%22:%22444%22,%22calc_method%22:%22Raw%22}],%22stages%22:[{%22name%22:%22Header%22,%22lat%22:{%22direction%22:%22North%22,%22degrees%22:48,%22minutes_input%22:%22AB.000%22},%22lon%22:{%22direction%22:%22East%22,%22degrees%22:16,%22minutes_input%22:%2200.ATMP%22}}],%22version%22:%22V0_1%22}
        let variables = vec![
            Variable::from(("A", "1", VariableCalculationMethod::Raw)),
            Variable::from(("B", "2", VariableCalculationMethod::Raw)),
            Variable::from(("AB", "33", VariableCalculationMethod::Raw)),
            Variable::from(("ATMP", "444", VariableCalculationMethod::Raw)),
        ];
        let stage = Stage {
            name: "Final".to_string(),
            lat: Latitude::with(Direction::North, 42, "AB.000"),
            lon: Longitude::with(Direction::East, 42, "00.ATMP"),
        };

        let expected = Ok(Value::new(CalculatedCoordinate::new(
            CalculatedCoordinatePart::with(Direction::North, 42, 33, 000),
            CalculatedCoordinatePart::with(Direction::East, 42, 0, 444),
        )));
        let result = helper_calculate_single_stage(variables, stage);
        assert_eq!(expected, result, "Coordinate calculation failed");
    }

    #[test]
    fn coord_calc_decimal_result_for_stage() {
        // https://127.0.0.1:8080/#{%22id%22:%22D3FLT%22,%22name%22:%22Default%20cache%20name%22,%22variables%22:[{%22name%22:%22A%22,%22answer%22:%221%22,%22calc_method%22:%22Raw%22},{%22name%22:%22B%22,%22answer%22:%222%22,%22calc_method%22:%22Raw%22},{%22name%22:%22AB%22,%22answer%22:%2233%22,%22calc_method%22:%22Raw%22},{%22name%22:%22ATMP%22,%22answer%22:%22444%22,%22calc_method%22:%22Raw%22}],%22stages%22:[{%22name%22:%22Header%22,%22lat%22:{%22direction%22:%22North%22,%22degrees%22:48,%22minutes_input%22:%22AB.000%22},%22lon%22:{%22direction%22:%22East%22,%22degrees%22:16,%22minutes_input%22:%2200.ATMP%22}}],%22version%22:%22V0_1%22}
        let variables = vec![Variable::from((
            "A",
            "15000",
            VariableCalculationMethod::Raw,
        ))];
        let stage = Stage {
            name: "Final".to_string(),
            lat: Latitude::with(Direction::North, 42, "((2*A) - 2695) / 1000"),
            lon: Longitude::with(Direction::East, 42, "((2*A) - 852) / 1.000"),
        };

        let expected = Ok(Value::new(CalculatedCoordinate::new(
            CalculatedCoordinatePart::with(Direction::North, 42, 27, 305),
            CalculatedCoordinatePart::with(Direction::East, 42, 29, 148),
        )));
        let result = helper_calculate_single_stage(variables, stage);
        assert_eq!(expected, result, "Coordinate calculation failed");
    }

    #[test]
    fn coord_calc_overlapping_variables_stages() {
        let variables = vec![
            Variable::from(("A", "1", VariableCalculationMethod::Raw)),
            Variable::from(("B", "2", VariableCalculationMethod::Raw)),
        ];
        let stages = vec![
            Stage {
                name: "Header".to_string(),
                lat: Latitude::with(Direction::North, 42, "00.000"),
                lon: Longitude::with(Direction::East, 42, "00.000"),
            },
            Stage {
                name: "Stage 1".to_string(),
                lat: Latitude::with(Direction::North, 42, "N0 + A"),
                lon: Longitude::with(Direction::East, 42, "E0 + B"),
            },
            Stage {
                name: "Stage 2".to_string(),
                lat: Latitude::with(Direction::North, 42, "N1 + A"),
                lon: Longitude::with(Direction::East, 42, "E1 + B"),
            },
            Stage {
                name: "Stage 3".to_string(),
                lat: Latitude::with(Direction::North, 42, "N2 + A"),
                lon: Longitude::with(Direction::East, 42, "E2 + B"),
            },
            Stage {
                name: "Stage 4".to_string(),
                lat: Latitude::with(Direction::North, 42, "N3 + A"),
                lon: Longitude::with(Direction::East, 42, "E3 + B"),
            },
            Stage {
                name: "Stage 5".to_string(),
                lat: Latitude::with(Direction::North, 42, "N4 + A"),
                lon: Longitude::with(Direction::East, 42, "E4 + B"),
            },
            Stage {
                name: "Stage 6".to_string(),
                lat: Latitude::with(Direction::North, 42, "N5 + A"),
                lon: Longitude::with(Direction::East, 42, "E5 + B"),
            },
            Stage {
                name: "Stage 7".to_string(),
                lat: Latitude::with(Direction::North, 42, "N6 + A"),
                lon: Longitude::with(Direction::East, 42, "E6 + B"),
            },
            Stage {
                name: "Stage 8".to_string(),
                lat: Latitude::with(Direction::North, 42, "N7 + A"),
                lon: Longitude::with(Direction::East, 42, "E7 + B"),
            },
            Stage {
                name: "Stage 9".to_string(),
                lat: Latitude::with(Direction::North, 42, "N8 + A"),
                lon: Longitude::with(Direction::East, 42, "E8 + B"),
            },
            Stage {
                name: "Stage 10".to_string(),
                lat: Latitude::with(Direction::North, 42, "N9 + A"),
                lon: Longitude::with(Direction::East, 42, "E9 + B"),
            },
            Stage {
                name: "Stage 11".to_string(),
                lat: Latitude::with(Direction::North, 42, "N10 + A"),
                lon: Longitude::with(Direction::East, 42, "E10 + B"),
            },
            Stage {
                name: "Stage 12".to_string(),
                lat: Latitude::with(Direction::North, 42, "N11 + A"),
                lon: Longitude::with(Direction::East, 42, "E11 + B"),
            },
            Stage {
                name: "Final (stage 13)".to_string(),
                lat: Latitude::with(Direction::North, 42, "N12 + A"),
                lon: Longitude::with(Direction::East, 42, "E12 + B"),
            },
        ];
        let number_stages = stages.len();

        let expected = vec![
            CalculatedCoordinate::new(
                CalculatedCoordinatePart::with(Direction::North, 42, 0, 0),
                CalculatedCoordinatePart::with(Direction::East, 42, 0, 0),
            ),
            CalculatedCoordinate::new(
                CalculatedCoordinatePart::with(Direction::North, 42, 0, 1),
                CalculatedCoordinatePart::with(Direction::East, 42, 0, 2),
            ),
            CalculatedCoordinate::new(
                CalculatedCoordinatePart::with(Direction::North, 42, 0, 2),
                CalculatedCoordinatePart::with(Direction::East, 42, 0, 4),
            ),
            CalculatedCoordinate::new(
                CalculatedCoordinatePart::with(Direction::North, 42, 0, 3),
                CalculatedCoordinatePart::with(Direction::East, 42, 0, 6),
            ),
            CalculatedCoordinate::new(
                CalculatedCoordinatePart::with(Direction::North, 42, 0, 4),
                CalculatedCoordinatePart::with(Direction::East, 42, 0, 8),
            ),
            CalculatedCoordinate::new(
                CalculatedCoordinatePart::with(Direction::North, 42, 0, 5),
                CalculatedCoordinatePart::with(Direction::East, 42, 0, 10),
            ),
            CalculatedCoordinate::new(
                CalculatedCoordinatePart::with(Direction::North, 42, 0, 6),
                CalculatedCoordinatePart::with(Direction::East, 42, 0, 12),
            ),
            CalculatedCoordinate::new(
                CalculatedCoordinatePart::with(Direction::North, 42, 0, 7),
                CalculatedCoordinatePart::with(Direction::East, 42, 0, 14),
            ),
            CalculatedCoordinate::new(
                CalculatedCoordinatePart::with(Direction::North, 42, 0, 8),
                CalculatedCoordinatePart::with(Direction::East, 42, 0, 16),
            ),
            CalculatedCoordinate::new(
                CalculatedCoordinatePart::with(Direction::North, 42, 0, 9),
                CalculatedCoordinatePart::with(Direction::East, 42, 0, 18),
            ),
            CalculatedCoordinate::new(
                CalculatedCoordinatePart::with(Direction::North, 42, 0, 10),
                CalculatedCoordinatePart::with(Direction::East, 42, 0, 20),
            ),
            CalculatedCoordinate::new(
                CalculatedCoordinatePart::with(Direction::North, 42, 0, 11),
                CalculatedCoordinatePart::with(Direction::East, 42, 0, 22),
            ),
            CalculatedCoordinate::new(
                CalculatedCoordinatePart::with(Direction::North, 42, 0, 12),
                CalculatedCoordinatePart::with(Direction::East, 42, 0, 24),
            ),
            CalculatedCoordinate::new(
                CalculatedCoordinatePart::with(Direction::North, 42, 0, 13),
                CalculatedCoordinatePart::with(Direction::East, 42, 0, 26),
            ),
        ];
        let (_, stages, _, _) = helper_calculate_multi(variables, stages);

        let mut count = 0;
        for (expected, result) in expected.into_iter().zip(stages.into_iter()) {
            assert_eq!(
                Ok(Value::new(expected)),
                result.1,
                "Failed for {}",
                result.0.name
            );
            count += 1;
        }
        assert_eq!(number_stages, count, "Not all stages checked!?");
    }

    #[test]
    fn coord_calc_stage_with_stage_vars() {
        // http://127.0.0.1:8080/#{%22id%22:%22D3FLT%22,%22name%22:%22Default%20cache%20name%22,%22variables%22:[{%22name%22:%22E%22,%22answer%22:%227%22,%22calc_method%22:%22Raw%22}],%22stages%22:[{%22name%22:%22Header%22,%22lat%22:{%22direction%22:%22North%22,%22degrees%22:42,%22minutes_input%22:%2210.000%22},%22lon%22:{%22direction%22:%22East%22,%22degrees%22:42,%22minutes_input%22:%2210.000%22}},{%22name%22:%22Stage%201%22,%22lat%22:{%22direction%22:%22North%22,%22degrees%22:48,%22minutes_input%22:%22N0%20+%20E%22},%22lon%22:{%22direction%22:%22East%22,%22degrees%22:16,%22minutes_input%22:%22E0%20+%20E%22}}],%22version%22:%22V0_1%22}
        let variables = vec![Variable::from(("E", "7", VariableCalculationMethod::Raw))];
        let stages = vec![
            Stage {
                name: "Header".to_string(),
                lat: Latitude::with(Direction::North, 42, "10.000"),
                lon: Longitude::with(Direction::East, 42, "10.000"),
            },
            Stage {
                name: "Final".to_string(),
                lat: Latitude::with(Direction::North, 42, "N0 + E"),
                lon: Longitude::with(Direction::East, 42, "E0 + E"),
            },
        ];

        let expected_final = Ok(Value::new(CalculatedCoordinate::new(
            CalculatedCoordinatePart::with(Direction::North, 42, 10, 7),
            CalculatedCoordinatePart::with(Direction::East, 42, 10, 7),
        )));

        let (_, stages, _, _) = helper_calculate_multi(variables, stages);
        let result = &stages[1].1;

        assert_eq!(*result, expected_final, "Coordinate calculation failed");
    }

    #[test]
    fn coord_calc_stage_with_sum() {
        // http://127.0.0.1:8080/#{%22id%22:%22D3FLT%22,%22name%22:%22Default%20cache%20name%22,%22variables%22:[{%22name%22:%22E%22,%22answer%22:%227%22,%22calc_method%22:%22Raw%22}],%22stages%22:[{%22name%22:%22Header%22,%22lat%22:{%22direction%22:%22North%22,%22degrees%22:42,%22minutes_input%22:%2210.000%22},%22lon%22:{%22direction%22:%22East%22,%22degrees%22:42,%22minutes_input%22:%2210.000%22}},{%22name%22:%22Stage%201%22,%22lat%22:{%22direction%22:%22North%22,%22degrees%22:48,%22minutes_input%22:%22N0%20+%20E%22},%22lon%22:{%22direction%22:%22East%22,%22degrees%22:16,%22minutes_input%22:%22E0%20+%20E%22}}],%22version%22:%22V0_1%22}
        let variables = vec![
            Variable::from(("A", "1", VariableCalculationMethod::Raw)),
            Variable::from(("B", "2", VariableCalculationMethod::Raw)),
            Variable::from(("C", "3", VariableCalculationMethod::Raw)),
            Variable::from(("D", "4", VariableCalculationMethod::Raw)),
            Variable::from(("SU", "5", VariableCalculationMethod::Raw)),
            Variable::from(("SUMMER", "6", VariableCalculationMethod::Raw)),
        ];
        let stages = vec![
            Stage {
                name: "Header".to_string(),
                lat: Latitude::with(Direction::North, 42, "10.000"),
                lon: Longitude::with(Direction::East, 42, "10.000"),
            },
            Stage {
                name: "Stage 1".to_string(),
                lat: Latitude::with(Direction::North, 42, "N0 + SUM"),
                lon: Longitude::with(Direction::East, 42, "E0 + SUMMER"),
            },
            Stage {
                name: "Stage 2".to_string(),
                lat: Latitude::with(Direction::North, 42, "N0 + SU"),
                lon: Longitude::with(Direction::East, 42, "E0 + SUM"),
            },
        ];

        let expected_s1 = Ok(Value::new(CalculatedCoordinate::new(
            CalculatedCoordinatePart::with(Direction::North, 42, 10, 21),
            CalculatedCoordinatePart::with(Direction::East, 42, 10, 6),
        )));
        let expected_s2 = Ok(Value::new(CalculatedCoordinate::new(
            CalculatedCoordinatePart::with(Direction::North, 42, 10, 5),
            CalculatedCoordinatePart::with(Direction::East, 42, 10, 21),
        )));

        let (_, stages, _, _) = helper_calculate_multi(variables, stages);

        let result = &stages[1].1;
        assert_eq!(
            *result, expected_s1,
            "Coordinate calculation failed (stage 1)"
        );

        let result = &stages[2].1;
        assert_eq!(
            *result, expected_s2,
            "Coordinate calculation failed (stage 2)"
        );
    }

    /// test case for issue #41
    #[test]
    fn coord_calc_subminutes_substitution_two_digits() {
        let variables = vec![
            Variable::from(("A", "99", VariableCalculationMethod::Raw)),
            Variable::from(("B", "099", VariableCalculationMethod::Raw)),
        ];
        let stages = vec![Stage {
            name: "Stage 1".to_string(),
            lat: Latitude::with(Direction::North, 42, "11.A"),
            lon: Longitude::with(Direction::East, 42, "11.B"),
        }];

        let expected = Ok(Value::new(CalculatedCoordinate::new(
            CalculatedCoordinatePart::with(Direction::North, 42, 11, 99),
            CalculatedCoordinatePart::with(Direction::East, 42, 11, 99),
        )));

        let (_, stages, _, _) = helper_calculate_multi(variables, stages);

        let result = &stages[0].1;
        assert_eq!(expected, *result, "Coordinate calculation failed (stage 1)");
    }

    /// test case for issue #41
    #[test]
    fn coord_calc_subminutes_substitution_one_digit() {
        let variables = vec![
            Variable::from(("A", "7", VariableCalculationMethod::Raw)),
            Variable::from(("B", "07", VariableCalculationMethod::Raw)),
            Variable::from(("C", "007", VariableCalculationMethod::Raw)),
        ];
        let stages = vec![
            Stage {
                name: "Stage 1".to_string(),
                lat: Latitude::with(Direction::North, 42, "11.A"),
                lon: Longitude::with(Direction::East, 42, "11.B"),
            },
            Stage {
                name: "Stage 2".to_string(),
                lat: Latitude::with(Direction::North, 42, "11.C"),
                lon: Longitude::with(Direction::East, 42, "11.000"),
            },
        ];

        let expected_s1 = Ok(Value::new(CalculatedCoordinate::new(
            CalculatedCoordinatePart::with(Direction::North, 42, 11, 7),
            CalculatedCoordinatePart::with(Direction::East, 42, 11, 7),
        )));
        let expected_s2 = Ok(Value::new(CalculatedCoordinate::new(
            CalculatedCoordinatePart::with(Direction::North, 42, 11, 7),
            CalculatedCoordinatePart::with(Direction::East, 42, 11, 0),
        )));

        let (_, stages, _, _) = helper_calculate_multi(variables, stages);

        let result = &stages[0].1;
        assert_eq!(
            expected_s1, *result,
            "Coordinate calculation failed (stage 1)"
        );
        let result = &stages[1].1;
        assert_eq!(
            expected_s2, *result,
            "Coordinate calculation failed (stage 2)"
        );
    }

    #[test]
    fn coord_calc_brackets() {
        let variables = vec![
            Variable::from(("A", "1", VariableCalculationMethod::Raw)),
            Variable::from(("B", "2", VariableCalculationMethod::Raw)),
            Variable::from(("C", "3", VariableCalculationMethod::Raw)),
            Variable::from(("D", "4", VariableCalculationMethod::Raw)),
            Variable::from(("E", "5", VariableCalculationMethod::Raw)),
            Variable::from(("F", "6", VariableCalculationMethod::Raw)),
            Variable::from(("G", "7", VariableCalculationMethod::Raw)),
            Variable::from(("H", "8", VariableCalculationMethod::Raw)),
            Variable::from(("I", "9", VariableCalculationMethod::Raw)),
        ];

        let stages = vec![
            Stage {
                name: "Stage 1".to_string(),
                lat: Latitude::with(Direction::North, 42, "11.(A+B)(C+D)(F-E)"),
                lon: Longitude::with(Direction::East, 42, "(F+I).(A+B)(C+D)(F-E"), // missing closing bracket on purpose
            },
            Stage {
                name: "Stage 2".to_string(),
                lat: Latitude::with(Direction::North, 42, "11.(F*G)I"),
                lon: Longitude::with(Direction::East, 42, "11.(G*H*I"), // missing closing bracket on purpose
            },
        ];

        let expected_s1 = Ok(Value::new(CalculatedCoordinate::new(
            CalculatedCoordinatePart::with(Direction::North, 42, 11, 371),
            CalculatedCoordinatePart::with(Direction::East, 42, 15, 371),
        )));
        let expected_s2 = Ok(Value::new(CalculatedCoordinate::new(
            CalculatedCoordinatePart::with(Direction::North, 42, 11, 429),
            CalculatedCoordinatePart::with(Direction::East, 42, 11, 504),
        )));

        let (_, stages, _, _) = helper_calculate_multi(variables, stages);

        let result = &stages[0].1;
        assert_eq!(
            expected_s1, *result,
            "Coordinate calculation failed (stage 1)"
        );
        let result = &stages[1].1;
        assert_eq!(
            expected_s2, *result,
            "Coordinate calculation failed (stage 2)"
        );
    }

    #[test]
    fn multicache_add_variable_single_overflow() {
        let mut multi = MultiCache::default();

        for __ in 1..=27 {
            multi.add_variable();
        }

        let variable_names: Vec<_> = multi.variables.iter().map(|var| var.name()).collect();
        let expected = [
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q",
            "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA",
        ]
        .to_vec();

        assert_eq!(expected, variable_names);
    }

    #[test]
    fn multicache_add_variable_overflow_double() {
        let mut multi = MultiCache::default();

        for __ in 1..=(2 * 26 + 1) {
            multi.add_variable();
        }

        let variable_names: Vec<_> = multi.variables.iter().map(|var| var.name()).collect();
        let expected = [
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q",
            "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG",
            "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU",
            "AV", "AW", "AX", "AY", "AZ", "BA",
        ]
        .to_vec();

        assert_eq!(expected, variable_names);
    }

    #[test]
    fn multicache_add_variable_overflow_many() {
        // This test case adds 1000 variables to a cache (without renaming any) and then checks if they all have unique
        // names. At the same time this also checks whether there are any crashes
        let mut multi = MultiCache::default();

        // should be more than we ever need (and it already takes a few seconds on my hardware)
        let iterations = 1_000;
        for __ in 1..=iterations {
            multi.add_variable();
        }

        //let variable_count = multi.variables.iter().filter(|var| !var.name().is_empty()).count();
        let variables: std::collections::HashSet<_> = multi
            .variables
            .iter()
            .map(|var| var.name().to_string())
            .collect();
        assert_eq!(iterations, variables.len());
    }

    #[test]
    fn multicache_add_variable_soundness_with_existing_and_renames() {
        let mut multi = MultiCache::default();
        multi.variables = vec![
            Variable::from(("A", "0", VariableCalculationMethod::Raw)),
            Variable::from(("B", "0", VariableCalculationMethod::Raw)),
            Variable::from(("C", "0", VariableCalculationMethod::Raw)),
            Variable::from(("AA", "0", VariableCalculationMethod::Raw)),
            Variable::from(("A1", "0", VariableCalculationMethod::Raw)),
        ];

        multi.add_variable(); // D
        multi.add_variable(); // E

        let variable_names: Vec<_> = multi.variables.iter().map(|var| var.name()).collect();
        let expected = ["A", "B", "C", "AA", "A1", "D", "E"].to_vec();
        assert_eq!(expected, variable_names);

        multi.variables[0].set_name("G".to_string());
        multi.add_variable(); // should add A after E
        multi.add_variable(); // should add F after new A
        multi.add_variable(); // should add H at the end

        let variable_names: Vec<_> = multi.variables.iter().map(|var| var.name()).collect();
        let expected = ["G", "B", "C", "AA", "A1", "D", "E", "A", "F", "H"].to_vec();
        assert_eq!(expected, variable_names);
    }

    #[test]
    fn multi_verify_checksum() {
        let mut multi = MultiCache::default();
        multi.variables = vec![
            Variable::from(("A", "1", VariableCalculationMethod::Raw)),
            Variable::from(("B", "Bob", VariableCalculationMethod::SumOfDigits)),
            Variable::from(("C", "5", VariableCalculationMethod::Raw)),
            Variable::from(("D", "X", VariableCalculationMethod::Raw)),
            Variable::from(("E", "=A+C", VariableCalculationMethod::Formula)),
            Variable::from(("X", "geocaching", VariableCalculationMethod::NumberOfDigits)),
            Variable::from(("F", "", VariableCalculationMethod::Raw)),
        ];
        multi.checksum = Some(Checksum::Sum(41));
        multi.stages = vec![
            Stage {
                name: "Header".to_string(),
                lat: Latitude::with(Direction::North, 42, "00.000"),
                lon: Longitude::with(Direction::East, 42, "00.000"),
            },
            Stage {
                name: "Stage 1".to_string(),
                lat: Latitude::with(Direction::North, 42, "B.CAA"),
                lon: Longitude::with(Direction::East, 42, "E0 + X"),
            },
            Stage {
                name: "Stage 2".to_string(),
                lat: Latitude::with(Direction::North, 42, "N1 + D"),
                lon: Longitude::with(Direction::East, 42, "E1 - F"),
            },
        ];

        let calc_result = multi.calc_all();
        assert_eq!(
            Some(ChecksumResult::Pass(41, 41)),
            calc_result.checksum,
            "Checksum failed"
        );
    }

    /// tests whether the special "SUM" variable is correctly generated
    #[test]
    fn multi_verify_sum() {
        let mut multi = MultiCache::default();
        multi.variables = vec![
            Variable::from(("A", "1", VariableCalculationMethod::Raw)),
            Variable::from(("B", "Bob", VariableCalculationMethod::SumOfDigits)),
            Variable::from(("C", "5", VariableCalculationMethod::Raw)),
            Variable::from(("D", "X", VariableCalculationMethod::Raw)),
            Variable::from(("E", "=A+C", VariableCalculationMethod::Formula)),
            Variable::from(("X", "geocaching", VariableCalculationMethod::NumberOfDigits)),
            Variable::from(("F", "", VariableCalculationMethod::Raw)),
        ];
        multi.checksum = Some(Checksum::Sum(41));
        multi.stages = vec![
            Stage {
                name: "Header".to_string(),
                lat: Latitude::with(Direction::North, 42, "00.000"),
                lon: Longitude::with(Direction::East, 42, "00.000"),
            },
            Stage {
                name: "Stage 1".to_string(),
                lat: Latitude::with(Direction::North, 42, "B.CAA"),
                lon: Longitude::with(Direction::East, 42, "E0 + X"),
            },
            Stage {
                name: "Stage 2".to_string(),
                lat: Latitude::with(Direction::North, 42, "N1 + D"),
                lon: Longitude::with(Direction::East, 42, "E1 - F"),
            },
        ];

        let calc_result = multi.calc_all();
        let sum = calc_result.get_sum();
        assert_eq!(41, sum.value(), "Sum doesnt match expected value");

        let sum_by_name = calc_result
            .all_variables_list
            .iter()
            .find(|(name, _, _)| name == "SUM");
        assert!(sum_by_name.is_some(), "No variable named SUM found");
    }

    /// Tests that even if a user adds a custom variable named "SUM", calc_result.get_sum() still returns the correct
    /// special sum variable. Additionally, the value of the user supplied variable should **not** be used for the
    /// calculation and should not end up in the variable list.
    #[test]
    fn multi_ensure_special_sum_precedence() {
        let mut multi = MultiCache::default();
        multi.variables = vec![
            Variable::from(("A", "1", VariableCalculationMethod::Raw)),
            Variable::from(("B", "Bob", VariableCalculationMethod::SumOfDigits)),
            Variable::from(("C", "5", VariableCalculationMethod::Raw)),
            Variable::from(("D", "X", VariableCalculationMethod::Raw)),
            Variable::from(("E", "=A+C", VariableCalculationMethod::Formula)),
            Variable::from(("X", "geocaching", VariableCalculationMethod::NumberOfDigits)),
            Variable::from(("F", "", VariableCalculationMethod::Raw)),
            Variable::from(("SUM", "99", VariableCalculationMethod::Raw)),
        ];
        multi.checksum = Some(Checksum::Sum(41));
        multi.stages = vec![
            Stage {
                name: "Header".to_string(),
                lat: Latitude::with(Direction::North, 42, "00.000"),
                lon: Longitude::with(Direction::East, 42, "00.000"),
            },
            Stage {
                name: "Stage 1".to_string(),
                lat: Latitude::with(Direction::North, 42, "B.CAA"),
                lon: Longitude::with(Direction::East, 42, "E0 + X"),
            },
            Stage {
                name: "Stage 2".to_string(),
                lat: Latitude::with(Direction::North, 42, "N1 + D"),
                lon: Longitude::with(Direction::East, 42, "E1 - F"),
            },
        ];

        let calc_result = multi.calc_all();

        let sum = calc_result.get_sum();
        assert_eq!(41, sum.value(), "Sum doesnt match expected value");

        let sum_by_name = calc_result
            .all_variables_list
            .iter()
            .find(|(name, _, _)| name == "SUM");
        assert!(sum_by_name.is_some(), "No variable named SUM found");
        let expected_sum = ("SUM".to_string(), Value::new(41), VariableKind::Sum);
        assert_eq!(expected_sum, *sum_by_name.unwrap(), "Unexpected sum");
    }

    fn helper_calculate_single_stage(
        variables: Vec<Variable>,
        stage: Stage,
    ) -> Result<Value<CalculatedCoordinate>, CoordinateError> {
        let multi = MultiCache {
            id: GeoCode::new("TEST".to_string()),
            name: "Test cache".to_string(),
            variables,
            stages: vec![stage],
            checksum: None,
        };

        let result = multi.calc_all();
        result.stages[0].1.clone()
    }

    fn helper_calculate_multi(
        variables: Vec<Variable>,
        stages: Vec<Stage>,
    ) -> (
        Vec<(Variable, Result<Value<i32>, String>)>,
        Vec<(Stage, Result<Value<CalculatedCoordinate>, CoordinateError>)>,
        VecDeque<(String, Value<i32>, VariableKind)>,
        Option<ChecksumResult>,
    ) {
        let multi = MultiCache {
            id: GeoCode::new("TEST".to_string()),
            name: "Test cache".to_string(),
            variables,
            stages,
            checksum: None,
        };

        let MultiCacheCalcResult {
            variables,
            stages,
            all_variables_list,
            checksum,
        } = multi.calc_all();
        let variables = variables
            .into_iter()
            .map(|(var, result)| (var.to_owned(), result))
            .collect();
        let stages = stages
            .into_iter()
            .map(|(stage, result)| (stage.to_owned(), result))
            .collect();
        (variables, stages, all_variables_list, checksum)
    }
}
