use std::collections::VecDeque;

use super::{calcwrapper, variable::VariableKind, value::Value};
use serde::{Serialize, Deserialize};

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub enum Checksum {
    Sum(i32),
    Formula(String, String),
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub enum ChecksumResult {
    Pass(i32, i32),
    Fail(i32, i32),
    FormulaError,
}

impl Checksum {
    pub fn check<S: AsRef<str>>(&self, variables: &VecDeque<(S, Value<i32>, VariableKind)>) -> ChecksumResult {
        match self {
            Self::Sum(sum_target) => {
                // first, lets try to find the sum by finding the special sum variable. If that doesnt work (for
                // whatever reason), we'll calculate the sum the old-fashioned way manually
                let sum_actual = match variables.iter().find(|(_, _, kind)| *kind == VariableKind::Sum) {
                    Some((_, value, _)) => value.value(),
                    None => {
                        log::warn!("No sum variable found. Calculating manually.");
                        variables.iter()
                            .map(|(_, value, kind)| {
                                if *kind == VariableKind::Variable {
                                    value.value()
                                } else {
                                    0
                                }
                            })
                            .sum::<i32>()
                    }
                };

                if sum_actual == *sum_target {
                    ChecksumResult::Pass(sum_actual, *sum_target)
                } else {
                    ChecksumResult::Fail(sum_actual, *sum_target)
                }
            }
            Self::Formula(left, right) => {
                let mut left = left.clone();
                let mut right = right.clone();

                for (var, value, _) in variables {
                    left = left.replace(var.as_ref(), &value.value().to_string());
                    right = right.replace(var.as_ref(), &value.value().to_string());
                }

                let left = match calcwrapper::evaluate(&left) {
                    Ok(result) => result as i32,
                    Err(_) => return ChecksumResult::FormulaError,
                };
                let right = match calcwrapper::evaluate(&right) {
                    Ok(result) => result as i32,
                    Err(_) => return ChecksumResult::FormulaError,
                };

                if left == right {
                    ChecksumResult::Pass(left, right)
                } else {
                    ChecksumResult::Fail(left, right)
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::{assert_eq};

    #[test]
    fn simple_sums() {
        let variables: VecDeque<_> = [
            ("A", Value::new(1), VariableKind::Variable),
            ("B", Value::new(2), VariableKind::Variable),
            ("C", Value::new(3), VariableKind::Variable),
        ].into_iter().collect();

        assert_eq!(ChecksumResult::Pass(6, 6), Checksum::Sum(6).check(&variables));
        assert_eq!(ChecksumResult::Fail(6, 7), Checksum::Sum(7).check(&variables));
    }

    #[test]
    fn formula_and_constant() {
        let variables: VecDeque<_> = [
            ("A", Value::new(1), VariableKind::Variable),
            ("B", Value::new(2), VariableKind::Variable),
            ("C", Value::new(3), VariableKind::Variable),
        ].into_iter().collect();

        let checksum_correct_1 = Checksum::Formula("B+C-A".to_string(), "4".to_string());
        let checksum_correct_2 = Checksum::Formula("4".to_string(), "B+C-A".to_string());
        let checksum_wrong = Checksum::Formula("B+C-A".to_string(), "5".to_string());

        assert_eq!(ChecksumResult::Pass(4, 4), checksum_correct_1.check(&variables));
        assert_eq!(ChecksumResult::Pass(4, 4), checksum_correct_2.check(&variables));
        assert_eq!(ChecksumResult::Fail(4, 5), checksum_wrong.check(&variables));
    }

    #[test]
    fn two_formulas() {
        let variables: VecDeque<_> = [
            ("A", Value::new(1), VariableKind::Variable),
            ("B", Value::new(2), VariableKind::Variable),
            ("C", Value::new(3), VariableKind::Variable),
            ("D", Value::new(4), VariableKind::Variable),
        ].into_iter().collect();

        let checksum_correct = Checksum::Formula("A+D".to_string(), "B+C".to_string());
        let checksum_wrong = Checksum::Formula("A+B".to_string(), "C+D".to_string());

        assert_eq!(ChecksumResult::Pass(5, 5), checksum_correct.check(&variables));
        assert_eq!(ChecksumResult::Fail(3, 7), checksum_wrong.check(&variables));
    }

    #[test]
    fn formula_missing_vars() {
        let variables: VecDeque<_> = [
            ("A", Value::new(1), VariableKind::Variable),
            ("B", Value::new(2), VariableKind::Variable),
        ].into_iter().collect();

        let checksum_1 = Checksum::Formula("A+B+C".to_string(), "3".to_string());
        let checksum_2 = Checksum::Formula("A+B".to_string(), "C+D".to_string());

        assert_eq!(ChecksumResult::FormulaError, checksum_1.check(&variables));
        assert_eq!(ChecksumResult::FormulaError, checksum_2.check(&variables));
    }
}
