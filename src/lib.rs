use yew::{html, Component, Context, Html};

mod icon;
mod pages;
mod sanitize;
mod storage;
mod util;

use pages::multicalc::{MultiCalc, MultiCalcPage};
use util::NeqAssign;

/// Use `util::version_string` instead
const VERSION: &str = env!("CARGO_PKG_VERSION");
/// Use `util::version_string` instead
const GIT_REVISION: Option<&'static str> = option_env!("CI_BUILD_REF");

pub struct Index {
    page: Page,
    multicalc: MultiCalc,
}

#[derive(PartialEq)]
pub enum Page {
    Index,
    MultiCalc,
}

pub enum IndexMsg {
    SwitchPage(Page),
}

impl Component for Index {
    type Message = IndexMsg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            page: Page::MultiCalc,
            multicalc: MultiCalc::load(),
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Self::Message::SwitchPage(page) => self.page.neq_assign(page),
        }
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        let screen = web_sys::window().unwrap().screen().unwrap();

        html! {
            <>
            <div style="height: 100%;">
                //{self.draw_navbar()}
                {self.draw_header()}

                {match self.page {
                    Page::Index => html!{
                        <p>{format!("Screen size: WxH {} x {}", screen.avail_width().unwrap(), screen.avail_height().unwrap())}</p>
                    },
                    Page::MultiCalc => html!{<MultiCalcPage data={self.multicalc.clone()} />}
                }}
            </div>

            <script src="./js/bootstrap.min.js"></script>

            <div id = "footer" class = "fixed-bottom bg-dark bg-gradient text-light fs-6">
                <span class = "float-end me-3">
                    {"| "}
                    <a  href = "https://gitlab.com/BafDyce/gc-web-tools"
                        class = "text-light"
                        rel = "noreferrer noopener"
                    >
                        {"Source Code @ GitLab"}
                    </a>
                    {" | "}
                    { util::version_string() }
                    {" | "}<a href="./changelog.html" class="text-light">{"Changelog"}</a>
                </span>
            </div>
            </>
        }
    }
}

impl Index {
    fn draw_header(&self) -> Html {
        html! {
            <h1 class="text-center">{"MultiCalc"}</h1>
        }
    }
}
