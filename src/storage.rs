//! Convenience functions for accessing local storage
use std::str::FromStr;

#[allow(unused_imports)]
use gloo_console::{debug, error};
#[allow(unused_imports)]
use wasm_bindgen::JsValue;
use web_sys::Storage;

#[derive(Debug)]
pub struct LocalStorage {
    storage: Storage,
}

#[derive(Debug)]
pub enum StorageItem {
    LatitudeDirection,
    LatitudeDegrees,
    LongitudeDirection,
    LongitudeDegrees,
}

impl LocalStorage {
    /// Panics if local storage cannot be accessed
    pub fn new() -> Self {
        Self {
            storage: web_sys::window()
                .expect("Unable to access window")
                .local_storage()
                .expect("Unable to access local_storage 1/2")
                .expect("Unable to access local_storage 2/2"),
        }
    }

    /// Retrieves the given item and parses it into the given data type, returning None on any error
    pub fn get<Data>(&self, item: StorageItem) -> Option<Data>
    where
        Data: FromStr + std::fmt::Debug,
        <Data as FromStr>::Err: std::fmt::Debug
    {
        //debug!("text", JsValue::from(format!("Loading data from LocalStorage: {}", item.index_name())));

        match self.storage.get_item(item.index_name()) {
            Ok(Some(data)) => {
                //debug!("text", JsValue::from(format!("> Data (raw): {}", data)));
                match data.parse() {
                    Ok(result) => {
                        //debug!("text", JsValue::from(format!("> Data (parsed): {:?}", result)));
                        Some(result)
                    },
                    Err(err) => {
                        error!("text", format!("> Failed to parse value: {:?}", err));
                        None
                    }
                }
            }
            _ => None,
        }
    }

    pub fn set<Data: ToString>(&self, item: StorageItem, data: Data) {
        //debug!("text", JsValue::from(format!("Setting {} to {}", item.index_name(), data.to_string())));
        let _ = self.storage.set(item.index_name(), &data.to_string());
    }
}

impl StorageItem {
    fn index_name(&self) -> &str {
        match self {
            Self::LatitudeDirection   => "LatitudeDirection",
            Self::LatitudeDegrees     => "LatitudeDegrees",
            Self::LongitudeDirection  => "LongitudeDirection",
            Self::LongitudeDegrees    => "LongitudeDegrees",
        }
    }
}
