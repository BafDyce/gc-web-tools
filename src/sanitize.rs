pub trait Sanitize {
    fn sanitized(self) -> (Self, Vec<(&'static str, FormatError)>) where Self: Sized;
}

#[derive(Debug, PartialEq)]
pub enum FormatError {
    InvalidValue,
    /// Value must start with a letter
    MustStartWithLetter,
    /// Only alpha numeric characters are alloed
    OnlyAlphaNumericAllowed,
}
