use std::fmt;

use crate::sanitize::{Sanitize, FormatError};

use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct GeoCode {
    id: String,
}

impl GeoCode {
    /// Creates a new GeoCode instance
    pub fn new(id: String) -> Self {
        Self { id }.sanitized().0
    }

    /// Sets the gc code of `self` to the new value but sanitized. Reports any errors but always overrides the old value
    pub fn set_to(&mut self, new_id: String) -> Vec<FormatError> {
        let (new_self, errors) = Self { id: new_id }.sanitized();
        *self = new_self;

        errors.into_iter().map(|(_, error)| error).collect()
    }

    /// Returns the inner value (without leading GC) as &str
    pub fn as_str(&self) -> &str {
        &self.id
    }

    /// Returns an URL to the cache
    pub fn url(&self) -> String {
        format!("https://coord.info/{}", self.id)
    }
}

impl fmt::Display for GeoCode {
    fn fmt(&self, ff: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(ff, "GC{}", self.id)
    }
}

impl Sanitize for GeoCode {
    fn sanitized(mut self) -> (Self, Vec<(&'static str, FormatError)>) {
        let mut only_alphanumeric_allowed_triggered = false;

        let code = self.id.chars()
            .filter_map(|cc| {
                match cc {
                    '0' ..= '9' | 'A' ..= 'Z' => Some(cc),
                    'a' ..= 'z' => Some(cc.to_ascii_uppercase()),
                    _ => {
                        only_alphanumeric_allowed_triggered = true;
                        None
                    },
                }
            })
            .collect::<String>();
        self.id = match code.strip_prefix("GC") {
            Some(remainder) => remainder.to_string(),
            None => code,
        };

        let mut errors = Vec::new();
        if only_alphanumeric_allowed_triggered {
            errors.push(("id", FormatError::OnlyAlphaNumericAllowed));
        }

        (self, errors)
    }
}

pub trait NeqAssign {
    fn neq_assign(&mut self, new: Self) -> bool;
}

impl<T: PartialEq> NeqAssign for T {
    fn neq_assign(&mut self, new: T) -> bool {
        if self != &new {
            *self = new;
            true
        } else {
            false
        }
    }
}

/// Attempts to copy the given text to the clipboard and returns whether it succeeded.
pub async fn copy_to_clipboard(text: &str) -> bool {
    match web_sys::window() {
        Some(window) => {
            match window.navigator().clipboard() {
                Some(clipboard) => {
                    let _ = wasm_bindgen_futures::JsFuture::from(
                        clipboard.write_text(text)
                            .then2(
                                &wasm_bindgen::closure::Closure::wrap(Box::new(|_|{})),
                                &wasm_bindgen::closure::Closure::wrap(Box::new(|_|{})),
                            )
                    ).await;

                    true
                }
                None => false,
            }
        }
        None => false,
    }
}

pub fn version_string() -> &'static str {
    lazy_static! {
        static ref VERSION_STRING: String = format!(
            "{version}{revision}",
            version = crate::VERSION,
            revision = match crate::GIT_REVISION {
                Some(revision) => format!("-{}", &revision[0..7]),
                None => String::new(),
            }
        );
    }

    &VERSION_STRING
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::{assert_eq};

    #[test]
    fn geocode_display() {
        let data = [
            ("ASDF", "GCASDF"),                     // normal case
            ("A1B2C3", "GCA1B2C3"),                 // numbers and letters
            ("lower", "GCLOWER"),                   // lower case letters should be converted
            (" with  spaces   ", "GCWITHSPACES"),   // spaces should be removed
            (" ABC _!$#^&*(", "GCABC"),             // special characters should also be removed
        ];

        for (input, expected) in data {
            let geocode = GeoCode::new(input.to_string());
            let display_result = format!("{}", geocode);
            assert_eq!(expected, display_result, "Failed for input: {}", input);
        }
    }

    #[test]
    fn geocode_sanitized() {
        let data = [
            ("ASDF", "ASDF", vec![]),
            ("A1B2C3", "A1B2C3", vec![]),
            ("lower", "LOWER", vec![]),
            (" with  spaces   ", "WITHSPACES", vec![("id", FormatError::OnlyAlphaNumericAllowed)]),
            (" ABC _!$#^&*(", "ABC", vec![("id", FormatError::OnlyAlphaNumericAllowed)]),
            // test if leading 'GC' is correctly stripped
            ("GC", "", vec![]),
            ("GCASDF", "ASDF", vec![]),
            ("gcASDF", "ASDF", vec![]),
            ("GCASDFGC", "ASDFGC", vec![]),
            ("CGASDF", "CGASDF", vec![]),
            ("1GCASDF", "1GCASDF", vec![]),
        ];

        for (input, expected_id, expected_errors) in data {
            let geocode = GeoCode { id: input.to_string() };

            let (result, result_errors) = geocode.sanitized();
            assert_eq!(expected_id.to_string(), result.id, "Wrong id for input {}", input);
            assert_eq!(expected_errors, result_errors, "Wrong errors for input {}", input);
        }
    }

    #[test]
    fn geocode_set_to() {
        let base = GeoCode::new("BASE".to_string());
        let data = [
            ("ASDF", "GCASDF", "ASDF", vec![]),
            ("A1B2C3", "GCA1B2C3", "A1B2C3", vec![]),
            ("lower", "GCLOWER", "LOWER", vec![]),
            (" with  spaces   ", "GCWITHSPACES", "WITHSPACES", vec![FormatError::OnlyAlphaNumericAllowed]),
            (" ABC _!$#^&*(", "GCABC", "ABC", vec![FormatError::OnlyAlphaNumericAllowed]),
        ];

        for (input, expected_display, expected_id, expected_errors) in data {
            let mut geocode = base.clone();
            let result_errors = geocode.set_to(input.to_string());

            assert_eq!(expected_id, geocode.id, "Wrong id for input {}", input);
            assert_eq!(expected_display, format!("{}", geocode), "Wrong display for input {}", input);
            assert_eq!(expected_errors, result_errors, "Wrong errors for input {}", input);
        }
    }
}
