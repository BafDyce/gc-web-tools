# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Fixed
- Some miscalculation cases

### Added
- Changelog
- Special `SUM` variable which can be used in calculations
- User supplied variables with a name of `SUM` will be ignored for calculations
- Now supports formats like `12.(A+B)(C*D)(E-F)` without throwing a parsing error
- Creating new stages remembers last used values for direction & degrees of latitude and longitude

### Changed
- Collapsed checksum now shows current (and target) values
- Changelog: Make headers with version information more visible
- (internal) Upgraded to yew 0.19.x

## [0.2.3] - 2021-12-06
### Added
- Working version with a few features (all features from `0.1.0` to `0.2.3` will be summarized in this entry)
- Ability to add as many variables as needed
  - By default, a variable is named with the next free letter (or `AB` and so on if `A` to `Z` are taken)
  - Variables can be renamed
    - No length limit
    - Name cannot be empty
    - Allowed characters: `A-Z`, `0-9`
    - Name cannot start with digit
    - All names will be converted to uppercase
  - Answer for the variable can be set. This can be:
    - A number
    - A letter, word, sentence
    - A formula (string starting with `=`, e.g. `= A+B`)
  - Calculation method can be chosen (defaults to `As-is`)
    - Currently supported methods:
      - `As-is`
      - `Sum of digits` (e.g., `1742` becomes `14`; `abcd` becomes `10`)
      - `Iterated Sum of digits` (e.g., `1742` becomes `5`; `abcd` becomes `1`)
      - `Number of digits` (e.g., `1742` becomes `4`; `abcd` becomes `4`)
  - A warning will be shown if:
    - No answer is set
    - The calculation method fails (e.g., for formulas)
    - Two variables have the same name
    - The name of a variable matches the pattern of stage variables (see below)
  - Variables can be deleted again (must be confirmed via a confirmation dialog)
- A checksum can be enabled/disabled (disabled by default)
  - When enabled, the following modes are currently available:
    - `Sum`: Simply sums up all variables and checks against a user input
    - `Formula`: User can enter two formulas (**without** leading `=` character!) which will be checked if they result in the same value
      - Can be useful if you are using temporary variables which shouldnt be included in the checksum calculation
- Ability to add as many stages as needed
  - Each stage can have a name (by default, the first one is named `Header`, while all others are named `Stage X` where X is a number, starting with `1`)
  - For Latitutde (`Lat`) and Longitude (`Lon`), the following can be set:
    - Direction (default to `N` for Latitude, `E` for Longitude)
    - Degrees (default to `48` for Latitude, `016` for Longitude)
    - Minutes, in several formats:
      - A plain number (e.g., `12.345` or without a dot `12345` - in fact, dots (`.`) will simply be ignored)
      - A formula, without leading `=` (e.g., `12.345 + (A+B) * C`, `12.ABC`, `A.BCD`, `E0 + 17`, `N7 + A*3`)
        - Any format encountered "in the wild" should be supported. If you find a Multi-cache which isnt directly supported, please report it!
  - Each stage also makes available two variables in the usual format (`N0`, `S1`, `E7`, and so on)
    - Depending on the direction of Latitude/Longitude, and the number of the stage
  - Stages can be deleted (must be confirmed in a separate dialog as well). **Be aware** This **will** change the numbering of stages!
  - A warning will be shown, if MultiCalc fails to calculate the coordinates of a stage.
  - In the small stage preview box (when the stage is not selected), the clipboard button on the right can be used to copy the coordinates to the clipboard.
- Ability to set cache metadata (optional)
  - Will also show a link to the geocache
- All data is stored in the URL
  - When the browser (suddenly) reloads the page, none of your data will be lost
  - Allows sharing the cache details from device to device (e.g., configure it in the comfort of your home and send it to your mobile phone where you only need to enter the variable answers on the go)
  - The URL data is located in the **URI fragment** and **your browser will never send it to the server**
    - Therefore, the server does not know what caches you are doing (and what the solutions are)
  - Unfortunately, for now, the URL tends to become **very long** (and not very readable) due to this
    - We will work on this in the future!
  - **Warning**: Currently, **no guarantees** about the stability of the URL data will be made. It may happen at any point, that URLs from a specific version will not work with a later version anymore.
    - We will also implement a solution to this once we move towards version `1.0.0` of MultiCalc
- Plaintext notes
  - At the end of the page, all information is presented in a human readable format
  - It also has a button to copy the text shown to the clipboard
  - Perfect to export it to your personal geocache notes after finishing the multi (or when aborting it)
