# GC Web Tools

## Development requirements

```bash
cargo install trunk             # builds & packages the application
cargo install pulldown-cmark    # for building changelog html file
```

## How to

```
RUSTFLAGS=--cfg=web_sys_unstable_apis trunk build
```

More info: https://trunkrs.dev/

> (!) RUSTFLAGS=--cfg=web_sys_unstable_apis is necessary so that we can use the clipboard feature

## Development notes

You can use trunkrs' serve command to rebuild the app whenever you change something:

```
RUSTFLAGS=--cfg=web_sys_unstable_apis trunk serve
```

Afterwards, you can access the app here: http://127.0.0.1:8080

**Note**: The clipboard feature only works on sites accessed via https or on localhost/127.0.0.1.
Currently trunkrs doesnt support serving over TLS, therefore a workaround is needed when you want
to use/test this feature from another device (e.g., mobile phone):

**One time setup**

```bash
pip install --user updog # A simple python http server that supports TLS; https://pypi.org/project/updog/
# You also need to ensure that your other device can reach port 8443 (or whatever you chose below).
# Configure your firewall accordingly
```

**Serve via TLS**

```bash
cd dist/
updog --ssl --port 8443

# On your other device, access the app with the following URL:
https://<IP_OF_YOUR_PC>:8443/index.html?view # You will need to confirm the certificate warning
```
